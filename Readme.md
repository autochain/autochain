#AutoChain
Project Report
 
Group 5
Kuenza Lhaden (12210061)
Bijay Kumar Rai (12210042)
Tashi Wangdi (12210095)
Dechen Namgay (12210047)
Sonam Tshering (12210033)


June 8, 2023,
PRJ201
ROYAL UNIVERSITY OF BHUTAN GYALPOZHING COLLEGE OF INFORMATION TECHNOLOGY
 
CERTIFICATE
This is to certify that the BSc.IT. project report titled “AutoChain”, which is being submitted by Kuenza Lhaden (12210061),Dechen Namgay (12210047),Sonam Tshering (12210033),Vijay Kumar Rai (12210047) and Tashi Wangdi (12210095)the students of Bachelors of Computer Science(Blockchain), prepared during the academic year 2023 in partial fulfilment of the requirement for the award of the degree of Bachelor of Computer Science(Blockchain) is a record of the students work carried out at the Gyalpozhing College of Information Technology, Royal University of Bhutan, Gyalpozhing under my supervision and guidance. 

Mr. Sonam Tshering
(Project Guide)

Associate Lecturer BSc.IT
Gyalpozhing College of Information Technology

Acknowledgement
We appreciate Mr. Sonam Tshering’s assistance in helping us acquire requirements and record necessary articles throughout weekly assessments. Thanks to the comments and suggestions we received during the presentations and consultation meeting, we were able to modify the way we approach project development by merging the lessons taught in the daily lectures. We are therefore forever indebted to him for all of his assistance and direction. We are grateful to the college administration for providing us with all of the required tools. This enabled us to finish our project and inspired us to go above and beyond to figure out what else might be added to make it even more promising.

Table of Content
1. Introduction	5
1.1 Aim	5
1.2 Goals	5
1.3 Objectives	5
1.4 Scope	6
2. Literature Review:	6
3. Background of the Project:	8
4. Workflow	10
5. Requirements Specification	11
5.1 Functional Requirements	11
5.2 Non-Functional Requirements	12
6. System Design	12
7. Milestone	14
8. Work Details Report	15
9. Test Plans	20
10. References	21




1.	Introduction
1.1	Aim
The aim of this project for a blockchain-based vehicle ownership transfer is to provide a safe, effective, and transparent mechanism for transferring ownership of vehicles in a short period of time using blockchain technology. 
1.2	Goals
●	To develop a web application for vehicle ownership transfer based on blockchain technology.
●	To create a transparent and secure environment for vehicle ownership transfer by leveraging blockchain technology and smart contracts to ensure all transactions and data are verifiable and tamper-proof
1.3	Objectives
The objective of our project is to: 
●	 To reduce time and cost involved in the process. 
●	To reduce error, fraud, and tampering of data. 
●	To provide a transparent system that users can trust. 
●	To create a user-friendly platform that is easy to use and accessible to all users. 


1.4	Scope
System Scope 
●	Development of a decentralized platform: The project’s scope involves creating a decentralized platform that enables users to efficiently and securely transfer ownership of their vehicles. Blockchain technology will be used to build the platform, which offers a transparent and tamper-proof solution.
●	User interface: The scope of the project includes the design of a user-friendly interface that is available to all users. The interface will be created to enhance user experience overall and streamline the ownership transfer procedure
●	Smart contract development: The scope of the project includes the creation of smart contracts which simplify the ownership transfer process. The automated execution of these contracts will lessen the need for manual intervention and increase overall productivity.
●	Compliance with regulations: The scope of the project includes compliance with relevant laws and regulations related to vehicle ownership transfer, and data privacy. The platform will be designed to be legally sound and reduce the risk of regulatory fines or legal disputes. 
●	Testing: The scope of the project includes testing and deployment of the platform.
User Scope
●	The end users of the web application are the vehicle sellers and buyers. 

2.	Literature Review:
1. RSTA (Road safety transport authority) Road Safety and transport authority use mRSTA which is used to transfer the vehicle ownership which is mandatory in Bhutan. The RSTA is crucial in making sure that all transfers of vehicle ownership are completed in a fair and lawful way. This includes checking that all required paperwork such as a clearance certificate from the central authority, Royal Monetary of Bhutan stating that the loan on the vehicle has either been fully paid, the clearance from the court to avoid legal dispute, and making sure the car is safe to drive and meets all safety requirements, and confirming the persons engaged in the transfer are who they say they are. (RSTA, 2021) The RSTA online system of vehicle ownership transfer had received many positive responses in terms of efficiency since the ownership transfer was made mandatory, the offices were loaded during a covid time and a long time was taken for the process to be done. With the use of an online system the owner can transfer ownership without having to travel, and attaching the necessary documents was simpler and took considerably less time than with the conventional approach. However, the certificate that is given to the new owner as proof that the transfer was successful is not secure since it is printed on paper, it can be forged or a fraudulent copy made. Since the mRSTA is a centralized system, the ownership transfer does not offer access to the timeline of vehicle ownership transitions. The users of mRSTA have stated that the system’s frequent downtime for maintenance makes them less interested in transferring ownership.
2. Parivahan Sewa the Ministry of Road Transport and Highways (MoRTH), Government of India, established the web portal Parivahan Sewa to offer different online services pertaining to the transportation industry. The website provides a number of services, such as the ability to register a vehicle, apply for a driver’s license, pay road taxes, and change the vehicle ownership.Parivahan Sewa allows the seller and the buyer to transfer by completing the required paperwork and submitting it to the Regional Transport Office (RTO) where the vehicle is registered.A new registration certificate (RC) will be issued in the buyer’s name after they accept the online payment and transfer ownership of the car to them. (Ownership Transfer, 2022) However, Parivahan users are saying that the transfer process takes too long and that the certificate they issue to show the new ownership is opaque because it is paper-based and easily falsified which are similar to problems shared by Bhutanese users on mRSTA.

3.	Background of the Project:
The process of changing a person’s or organization’s legal ownership of a vehicle is known as a vehicle ownership transfer. There is a ton of paperwork involved in this procedure, which is frequently difficult and time-consuming. The transfer of car ownership, however, can be made easier, more effective, and safer with the development of blockchain technology. A distributed ledger technology called blockchain enables safe and open transactions without the need for a centralized authority. Participants can originate, confirm, and enforce transactions on a shared ledger using this decentralized technology. Since the ledger cannot be edited or deleted after a transaction has been recorded, it is impenetrable and tamper-proof. Blockchain technology can be utilized to build a safe and transparent platform for documenting and verifying ownership transfers in the context of the transfer of vehicles. This can be done by setting up a blockchain-based registration of car ownership that authorized parties, such the government, insurance providers, and vehicle owners, can access and amend. There are various advantages to using blockchain technology in the transfer of car ownership, such as providing real-time ownership verification, it can increase security and transparency by reducing disputes and fraud. Finally, it can give car owners more privacy by letting them manage who has access to their personal data. In general, the application of blockchain technology to the transfer of vehicle ownership has the potential to completely transform the process, making it easier, more effective, and more secure.


5.	Requirements Specification
5.1	Functional Requirements
1.          Registration
●	The user will click on register and enter their username, Gmail address, and password. Then they will get registered.

2.          Transfer of Ownership
●	The ownership should be transferable from one party to another through the system. Both ownership rights and registration data should be transferred in this process.
3.          Smart Contracts
●	Smart contracts can be used to automate the ownership transfer process while ensuring legitimacy and compliance with the conditions of the contract, resulting in a quicker and more trustworthy transfer.
 4.          Records of Transfer
●	For each car, the system should keep track of all ownership changes. This should contain the parties involved, the date of the transfer, and any other pertinent details.




5.2	Non-Functional Requirements
1.          Decentralization
Decentralization refers to the absence of a single point of control or failure in the system. Using a distributed ledger technology, such as blockchain, makes it possible to create an immutable and tamper-proof record of ownership transfers
2.          Security
The system should be built with security and defense against hacking and other sorts of assaults in mind.
 
3.  	Transparency
The procedure should be open and transparent. This implies that everyone involved in the ownership transfer procedure should have access to the transfer's specifics, including any fees or costs involved. This promotes mutual trust among the parties and guarantees that the transfer will be handled fairly and openly.
 
6.          Efficiency
The process needs to be effective and streamlined. This implies that as little manual intervention as feasible should be needed during the ownership transfer procedure.
 
7.          Scalability
Scalability refers to the system's ability to handle high volumes of ownership changes without sacrificing efficiency or security.
 
8.          Accessibility
Everyone participating in the ownership transfer procedure should have access to the system.
 
6.	System Design
The Vehicle Ownership transfer system architecture contains the following entities:
●	User Interface: Users can register, make profiles and transfer ownership of vehicles. The user experience will be created with accessibility and usability in mind. It is accessed via the blockchain browser, in our case Chrome plus Metamask extension, that runs on any device type such as phone, laptop, etc.

●	Blockchain: It is the heart of the proposed system which manages all transactions. The applied blockchain type is permissionless, i.e., public, that allows anyone to act as a user. In addition to transfer of ownership handling, blockchain supports keeping records of owners of vehicles.

●	Smart contracts: The business logic of the proposed vehicle ownership transfer system is carried out by smart contract entities which are executed by a blockchain network. AutoChain will use smart contracts to automate the process of transferring ownership of vehicles.


 
7.	Milestone
SL No.	Milestone	Description	Time/Date
1	Project Start	Define project title, project scope, objectives, and timelines	6 March
2	Requirement gathering and analysis	Identify user requirements and create a feature list	8 March
3	Design and architecture (Frontend)	Develop a blockchain-based architecture for Vehicle ownership transfer website	16 March
4	User registration and authentication	Implement a user registration and authentication process using blockchain	6 April
5	Ownership Transfer Request	Develop a mechanism for transfer request	21 April
6	Smart contracts implementation	Implement smart contracts to automate transfer processes	8 may
7	User feedback and rating	Create a feedback and rating system for users	15 may
7	Testing and quality assurance	Test the vehicle ownership transfer website and ensure that it meets quality standards	1 June
8	Launch	Launch the blockchain-based vehicle ownership transfer website and promote it	6 June
9	Maintenance and support	Provide ongoing maintenance and support for the website, including bug fixes and security updates	8 June


8.	Work Details Report

Date	Activity Name	Activity Description	Time
 
02/03/2023	 
Introduction	●	 Five of us came together for the first time, elected the group leader, and discussed potential project ideas.
●	 Decided we will be developing a web-based project
●	Decided to bring each topic ideas when we gather next time	4:00-5:00 pm
05/03/2023	Topic sharing and topic discussion	●	Came up with each topic and shared the ideas with the members
●	Sat together and Did some research on the topic to see whether it is applicable or not.
●	Out of 5 topics we couldn’t choose one, so we met our guide and asked for suggestions.	2:00 pm– 5:00 pm
06/03/2023	Topic selection	• Met with Sir and provided some suggestions on each area and other topics, like a blockchain-based voting system, leveraging blockchain technology to transfer vehicle ownership, and a decentralized certificate generator.	4:00pm-5:30 pm
07/03/2023	Conclusion for topic selection	●	We finally decided on the topic sir suggested that is vehicle ownership transfer.
●	Planned to do deep research on the topic	11am -11:43am
08/03/2023	Discussion with guide	●	Talked more about the ideas of our project and discussed the topics we have researched.	3:20pm– 3:50pm
08/03/2023	Proposal discussion	●	We had a short meeting to refer to the proposal template, so based on it we decided what to do next.
●	Divided the topics to research(for proposal)	8:15pm -9:35pm
11/03/2023	Proposal writing	●	We gathered to share our individual topic ideas and jotted it down on the proposal template.	10am – 1pm
12/03/2023	Proposal finalization	●	We sent our proposal link to sir before the submission for checking	 
12/03/2023	Proposal Edition	●	We edited the proposal according to our guide’s feedback	8:15pm-9:30pm
13/03/2023	Proposal submission	●	Submitted our proposal for evaluation	11:00pm
14/03/2023	Presentation discussion	●	We had briefs on presentation making and distributed our parts to do the presentation.	2:15pm – 3:00pm
17/03/2023	Schedule discussion	 • Since each of us had a different schedule, we virtually determined when to conduct the presentation. After consulting with our guide, we chose to conduct the presentation on March 21, 2023.	8:00pm- 8:30pm
21/03/2023	presentation	●	We presented our proposal to our guide in the guide’s office.	2:00pm – 3:00pm
23/03/2023	Wireframe discussion and wireframe designing	●	We talked about the design, sketched the wireframe by hand on paper, and then got down to develop the wireframe in Figma.
●	We also edited the proposal according to our guide’s feedback.	10:00am- 1:00pm
25/03/2023	Prototype discussion and prototype designing	●	We decided our color scheme
●	Also designed a logo for our website
●	Started designing our prototype	8:45pm-10:34pm
27/03/2023	Prototype design, ERD and user story discussion.	●	We revised our prototype, came up with a user story, and developed the RED.	3:00pm:4:00pm
28/03/2023	Discussion with guide	●	We met with Sir to present our prototype, ERD, and user story and receive feedback from Sir.	2:15pm-3:30pm
28/03/2023	Virtual meeting	●	 In accordance with the guides' feedback, we divided the edition that was required for the prototype.	9:00pm-9:40pm
3/04/2023	Schedule discussion	●	After consulting with our guide and examiner, we decided to do the CA2 presentation on 5th April.	1:00pm-1:20pm
5/04/2023	Presentation	●	We presented our high fidelity wireframe, user story and ERD to our guide and examiner Mr. Ronnie.	2:30pm-3:20pm
8/04/2023	Discussion with our guide	●	We met with our guide to review the examiner's feedback.
●	We have elected a new leader	9:00am-11am
9/04/2023	Front end design discussion	●	We divided the parts to edit in the prototype.
●	We divided the front end and assigned the task in asana	2:15pm-3:15pm
12/04/2023	Frontend updates and smart contract discussion	●	We have shown the front end design progress and got each other’s feedback and we also assigned task to develop smart contract for our project	12:00am-2:00pm
14/04/2023	Discussion with our guide	●	We met with sir to show our front end progress and got some feedback.	2:15-3:15pm
19/04/2023	Discussion with guide	●	We met with sir to show the updated design of the front end.	3:15-4:00pm
4/05/2023	Discussion with a guide.	●	We showed our final front end design to our guide before submitting.	2:15-3:00pm
6/05/2023	Discussion	●	We connected all the front ends.	6:30pm-10:30pm
15/05/2023	Presentation	●	We did our CA3 presentation	2:00-3:00pm
26/05/2023	Connection between frontend and backend	●	We connected our smart contract with the front end.	2:15pm-6:00pm
28/05/2023	Project progress to our guide	●	We have sent the project progress to our guide.	 
01/06/2023	Work distribution	●	We met and were assigned to work on documentation, poster and promotional video.	2:00pm-2:30pm



9. User Manual
●	The following would be a simple guide to navigate through the website and check out all the functionalities provided.
9.1 Home page
●	This is the homepage of the website, which contains the information about the platform and what service we provide.
 
9.2 SignUp page
●	After clicking on the login button, the sign-up page will be shown for users to register on our website. For that, users need to fill out the form entering their credentials.

9.3 Login
●	After signing up the user has to log in again to use our website.
 
9.4 Landing page
●	After login the user will be redirected to this landing page where the user can use our functionalities.

9.5 Vehicle Registration Page
●	This is the vehicle registration page, where you can register by entering the vehicle registration details.

9.6 My Vehicle Page.
●	Here you can see the vehicle that you have registered.
 
9.7 Vehicle Transfer page.
●	In this section, you can transfer the ownership of the vehicle by entering the vehicle id that was generated when registering your vehicle and the account address of the new vehicle owner.

9.8 Admin Dashboard.
●	This is the admin GUI when the admin gets logged in to the system.

9.9 Vehicle History.
●	This is the vehicle history for admins to view the history of the vehicle registered in our system.
 
9.10 Vehicle Transferred.
●	This is the GUI for the admin to view the Vehicle transferred page.

9.	References
Ownership Transfer | Parivahan Sewa | Ministry of Road Transport & Highways, Government of India. (2022, December 28). Parivahan Sewa. Retrieved March 10, 2023, from https://parivahan.gov.in/parivahan/en/content/ownership-transfer RSTA. (2016). RSTA. 
Retrieved March 10, 2023, from https://www.rsta.gov.bt/rstaweb/load.html?id=70&field_cons=PAGE

