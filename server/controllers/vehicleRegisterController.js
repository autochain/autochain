const vehicleRegister = require("./../models/vehicleRegisterModels")
const User = require("./../models/userModels")

const multer = require("multer")

const multerStorage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, "views/img/registeredVehicle"); // Specify the destination folder for vehicle photos
    },
    filename: (req, file, cb) => {
        const ext = file.mimetype.split("/")[1];
        cb(null, `vehicle-${req.params.id}-${Date.now()}.${ext}`); // Use a unique filename for each vehicle photo
    },
});

const multerFilter = (req, file, cb) => {
    if (file.mimetype.startsWith("image")) {
        cb(null, true);
    } else {
        cb(new AppError("Not an image! Please upload only images", 400), false);
    }
};

const upload = multer({
    storage: multerStorage,
    fileFilter: multerFilter,
});

exports.uploadVehiclePhoto = upload.single("image");


exports.getAllVehicles = async (req, res) => {
    try {
        const vehicleRegister1 = await
            vehicleRegister.find()
        res.status(200).json({
            status:
                "success", results:
                vehicleRegister1.length, data: {
                    vehicleRegister1,
                },
        })
    } catch (err) {
        res.status(404).json({
            status:
                "fail", message: "Invalid data sent!",
        })
    }
}


exports.getvehicles = async (req, res) => {
    try {
        const vehicle = await vehicleRegister.findById(req.params.id);

        if (!vehicle) {
            return res.status(404).json({
                status: "fail",
                message: "Vehicle not found",
            });
        }

        // Construct the image URL using the filename stored in the vehicle document
        const imageUrl = `http://localhost:4001/views/img/registeredVehicle/${vehicle.image}`;


        res.status(200).json({
            status: "success",
            data: {
                vehicle: {
                    ...vehicle.toObject(),
                    imageUrl: imageUrl,
                },
            },
        });
    } catch (err) {
        res.status(500).json({
            status: "fail",
            message: err,
        });
    }
};

exports.addVehicles = async (req, res) => {
    try {
        if (req.file) {
            req.body.image = req.file.filename;
        }

        const newVehicle = await vehicleRegister.create(req.body);

        // Retrieve the user based on the user ID
        const user = await User.findById(req.body.userId);

        if (!user) {
            return res.status(404).json({
                status: "fail",
                message: "User not found",
            });
        }

        // Push the new vehicle ID to the user's vehicle array
        user.vehicle.push(newVehicle._id);

        // Save the updated user document
        await user.updateOne(
            { _id: req.body.userId },
            { $push: { vehicle: newVehicle._id } }
        );

        res.status(201).json({
            status: "success",
            data: {
                vehicleRegister: newVehicle,
            },
        });
    } catch (err) {
        res.status(400).json({
            status: "fail",
            message: err.message,
        });
    }
};

exports.getUserWithVehicles = async (req, res) => {
    try {
      const userId = req.params.userId;
  
      const user = await User.findById(userId).populate('vehicle');
  
      if (!user) {
        return res.status(404).json({ message: 'User not found' });
      }
  
      res.status(200).json({ user });
    } catch (error) {
      console.error(error);
      res.status(500).json({ message: 'Failed to fetch user' });
    }
  };

exports.updateVehicles = async (req, res) => {
    try {
        const vehicleRegister1 = await vehicleRegister.findByIdAndUpdate(req.params.id, req.body, {
            new: true,
            runValidators: true,
        })
        res.status(200).json({
            status: "success", data: {
                vehicleRegister1,
            },
        })

    } catch (err) {
        res.status(404).json({
            status: "fail", message: err,
        })
    }
}
exports.deleteVehicles = async (req, res) => {
    try {
        const vehicleRegister1 = await vehicleRegister.findByIdAndDelete(req.params.id)
        res.status(200).json({
            status: "success", data: {
                vehicleRegister1,
            },
        })
    } catch (err) {
        res.status(404).json({
            status: "fail", message: err,
        })
    }
}