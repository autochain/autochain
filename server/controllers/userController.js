const User = require("./../models/userModels")
const AppError = require("../utils/appError");
const jwt = require('jsonwebtoken');
const multer = require("multer")

const multerStorage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, "views/img/users");
    },
    filename: (req, file, cb) => {
        let ext = "";
        if (file.mimetype) {
            ext = file.mimetype.split("/")[1];
        }
        if (req.token) {
            const obj = JSON.parse(req.token);
            cb(null, `user-${obj._id}-${Date.now()}.${ext}`);
        } else {
            ext = ".png";
            cb(null, `user-${Date.now()}.${ext}`);
        }
    },
});



const multerFilter = (req, file, cb) => {
    if (file.mimetype.startsWith("image")) {
        cb(null, true)
    } else {
        cb(new AppError("Not an image! Please upload only images", 400), false)
    }
}
const upload = multer({
    storage: multerStorage,
    fileFilter: multerFilter
})
exports.uploadUserPhoto = upload.single("image");


exports.getAllUsers = async (req, res, next) => {
    try {
        const users = await User.find()
        res.status(200).json({ data: users, status: "success" })
    } catch (err) {
        res.status(500).json({ error: err.message });
    }
}

exports.createUser = async (req, res) => {
    try {

        const user = await User.create(req.body);
        console.log(req.body.name)
        res.json({ data: user, status: "success" });
    } catch (err) {
        res.status(500).json({ error: err.message });
    }
}

exports.getUser = async (req, res) => {
    try {
        const user = await User.findById(req.params.id);
        res.json({ data: user, status: "success" });
    } catch (err) {
        res.status(500).json({ error: err.message });
    }
}

exports.updateUser = async (req, res) => {
    try {
        const user = await User.findByIdAndUpdate(req.params.id, req.body);
        res.json({ data: user, status: "success" });
    } catch (err) {
        res.status(500).json({ error: err.message });
    }
}

exports.deleteUser = async (req, res) => {
    try {
        const user = await User.findByIdAndDelete(req.params.id);
        res.json({ data: user, status: "success" });
    } catch (err) {
        res.status(500).json({ error: err.message });
    }
}
// Function to filter out unwanted fields
const filterObj = (obj, ...allowedFields) => {
    const filteredObj = {};
    Object.keys(obj).forEach((field) => {
        if (allowedFields.includes(field)) {
            filteredObj[field] = obj[field];
        }
    });
    return filteredObj;
};
exports.updateMe = async (req, res, next) => {
    try {
        // 1) Create an error if users post password data
        if (req.body.password || req.body.passwordConfirm) {
            return next(
                new AppError(
                    "This route is not for password updates. Please use /updateMyPassword",
                    400
                )
            );
        }

        // 2) Filter out unwanted fields that are not allowed to be updated
        const filteredBody = filterObj(req.body, "name", "email", "cid", "phonenumber");
        if (req.file) {
            filteredBody.image = req.file.filename;
        }

        // Access the token value from cookies
        const token = req.headers.authorization?.split(" ")[1];
        if (!token) {
            return next(new AppError("You are not logged in!", 401));
        }

        // Verify token and extract the user ID
        const decoded = jwt.verify(token, process.env.JWT_SECRET);
        const userId = decoded.id;

        const updatedUser = await User.findByIdAndUpdate(userId, filteredBody, {
            new: true,
            runValidators: true
        });

        res.status(200).json({
            status: "success",
            data: { user: updatedUser }
        });
    } catch (err) {
        res.status(500).json({ error: err.message });
    }
};

// Update user status
exports.updateUserStatus = async (req, res) => {
    try {
      const { userId } = req.params;
      const { active } = req.body;
  
      const user = await User.findByIdAndUpdate(
        userId,
        { active },
        { new: true }
      );
  
      if (!user) {
        return res.status(404).json({ error: "User not found" });
      }
  
      res.json({ data: user, status: "success" });
    } catch (err) {
      res.status(500).json({ error: err.message });
    }
  };