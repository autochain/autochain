const feedback = require("./../models/feedbackModels")
const validator = require("validator");

exports.getAllFeedbacks = async (req, res, next) => {
    try {
        const feedbacks = await feedback.find()
        res.status(200).json({ data: feedbacks, status: "success" })
    } catch (err) {
        res.status(500).json({ error: err.message });
    }
}

exports.getfeedback =async(req,res) => {
    try{
        const feedback1 =await
        feedback.findById(req.params.id)
      
        res.status(200).json({
            status:"success",
            results:vehicleRegister1.length,
            data:{
                feedback1,
            },
        })
    }catch(err){
        res.status(404).json({
            status: "fail", message: err.message,
        })
    }
}
exports.addfeedback = async (req, res) => {
    try{


        if (req.body.email && !validator.isEmail(req.body.email)) {
            throw new Error("Invalid email format");
        }
        const newfeedback =await feedback.create(req.body)
        res.status(201).json({
            status: "success", data: {
                Feedback: newfeedback,
            },
        })
    }catch(err){
        res.status(400).json({
            status: "fail", message: err,
        })
    }
} 

exports.updatefeedback =async (req,res) => {
    try{
        const feedback1 =await feedback.findByIdAndUpdate(req.params.id, req.body, {
            new: true,
            runValidators:true,
        })
        res.status(200).json({
            status: "success", data: {
                feedback1,
            },
        })

    }catch(err){
        res.status(404).json({
            status: "fail", message: err,
        })
    }
}
exports.deletefeedback =async (req,res) => {
    try{
        const feedback1 =await feedback.findByIdAndDelete(req.params.id)
        res.status(200).json({
            status:"success", data:{
                feedback1,
            },
        })
    }catch(err){
        res.status(404).json({
            status: "fail", message: err,
        })
    }
}