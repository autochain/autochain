const express=require("express");
const cookieParser =require("cookie-parser")
const mongoose = require("mongoose");
const dotenv = require("dotenv");

dotenv.config({ path: "./config.env" });
const app =express();
app.use(cookieParser());
const cors = require('cors');





// enable CORS
app.use(cors());



// middelware
app.use(express.urlencoded({extended:true}));
app.use(express.json());


const userRouter =require("./routes/userRoutes");
const transferRouter =require("./routes/transferRoutes");
const path =require("path");  

app.use("/api/v1/users",userRouter);
app.use("/api/v1/users",transferRouter);
app.use(express.static(path.join(__dirname,"views")));

const DB = process.env.DATABASE.replace( 'PASSWORD', process.env.DATABASE_PASSWORD );

mongoose.connect(DB).then((con)=>{
    console.log(con.connections)
    console.log("DB connection successful")
}).catch(error => console.log(error));


const port = process.env.port || 4001
app.listen(port, () => {
  console.log(`App running on port ${port} ..`)
})
