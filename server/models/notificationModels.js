const mongoose = require('mongoose');


const notificationSchema = new mongoose.Schema({
  _From: {
    type: String,
    required: true
  },
  vehicleId: {
    type: String,
    required: true
  },
  createdAt: {
    type: Date,
    default: Date.now
  }
});

const Notification = mongoose.model('Notification', notificationSchema);

module.exports = Notification;