const mongoose = require("mongoose")
const User = require('./userModels');

const vehicleRegisterSchema = new mongoose.Schema({
    vehiclenumber: {
        type: String,
        required: [true, 'Please provide your license number!'],

    },
    vehicletype: {
        type: String,
        required: [true, 'Please provide a Vehicle Type!'],

    },
    vehiclemodel: {
        type: String,
        required: [true, 'Please provide Vehicle Model!'],

    },
    vehiclecolor: {
        type: String,
        required: [true, 'Please provide your Vehicle Color!'],

    },
    image:{
        type: String,
        required: [true, "Please provide your Vehicle Image "],
    },
    active: {
        type: Boolean,           
        default: true,
        select: true,
    },
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true,
    },
})

vehicleRegisterSchema.pre(/^find/, function(next){
    this.populate({
        path: 'userId',
        select: 'name'
    })
    next()
})




const vehicleRegister = mongoose.model('vehicleRegister', vehicleRegisterSchema)
module.exports = vehicleRegister
