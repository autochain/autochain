const mongoose = require("mongoose")
const validator = require("validator")
const bcrypt = require("bcryptjs")
const VehicleRegister = require("./vehicleRegisterModels")

const userSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, "Please enter your name!"],
    },
    email: {
        type: String,
        required: [true, "Please provide your email"],
        unique: true,
        lowercase: true,
        validate: [validator.isEmail, "Please provide a valid Email"],
    },
    phoneNumber: {
        type: String,
        required: true,
    },
    cid: {
        type: String,
        required: true,
        minlength: 11,
    },
    accountAddress: {
        type: String,
        required: [true, "Please provide the account address"],
        unique: true,
    },
    password: {
        type: String,
        required: [true, "Please provide a password"],
        minlength: 8,
        // password wont be included when we get users
        select: false,
    },
    
    passwordConfirm: {
        type: String,
        required: [true, "Please confirm your password"],
        validate: {
            validator: function (el) {
                return el === this.password;
            },
            message: "Passwords are not the same",
        },
    },
    image:{
        type: String,
        required: [true, "Please provide your User Image "],
        default:"placeholder.png",
    },
    vehicle:[{
        type: mongoose.Schema.Types.ObjectId,
        ref:"VehicleRegister",
        select: true,
    }],
    
    active: {
        type: Boolean,
        default: true,
        select: true,
    },
})


// Save middleware
userSchema.pre('save', async function (next) {
    // Check if the accountAddress field is modified
    if (this.isModified('accountAddress')) {
      try {
        // Attempt to find another user with the same accountAddress
        const existingUser = await User.findOne({ accountAddress: this.accountAddress });
        if (existingUser) {
          // Throw an error if a user with the same accountAddress already exists
          throw new Error('Account address must be unique');
        }
      } catch (error) {
        // Handle the error appropriately (e.g., return an error response)
        return next(error);
      }
    }
  
    // Continue with the save operation
    next();
  });

userSchema.pre("save", async function (next) {
    if (!this.isModified("password")) return next()
    this.password = await bcrypt.hash(this.password, 12)
    this.passwordConfirm = undefined
    next()
})


userSchema.pre('findOneAndUpdate', async function (next) {
    const update = this.getUpdate();
    if (update.password !== '' &&
        update.password !== undefined &&
        update.password == update.passwordConfirm) {

        // Hash the password with cost of 12
        this.getUpdate().password = await bcrypt.hash(update.password, 12)

        // // Delete passwordConfirm field
        update.passwordConfirm = undefined
        next()
    } else
        next()
})

//instance method is available in all document of certain collections
userSchema.methods.correctPassword = async function (
    candidatePassword,
    userPassword,
) {

    return await bcrypt.compare(candidatePassword, userPassword)
}

const User = mongoose.model('User', userSchema)
module.exports = User
