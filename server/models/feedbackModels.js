const mongoose =require("mongoose")
const validator = require("validator")


const feedbackSchema =new mongoose.Schema({
    name: {
        type: String,
        required: [true,'Please tell us your name!'],
    },
    email: {
        type:String,
        required: [true, 'Please provide your email'],
        unique: true,
        lowercase: true,
        validate: [validator.isEmail, 'Please provide a valid email'],
    },
    PostDate:{
        type:Date,
        required:[true,"Please provide the post date of your feedback"],
    },
    reason: {
        type: String,
        required: [true, 'Please provide a reason!'],

    },
    active: {
        type: Boolean,
        default: true,
        select: false,
    },
})
const Feedback =mongoose.model('Feedback',feedbackSchema)
module.exports =Feedback
