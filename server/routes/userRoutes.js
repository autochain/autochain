
const express = require('express')
const userController = require('./../controllers/userController')
const authController = require('./../controllers/authController')
const jwt = require('jsonwebtoken');
const vehicleRegisterController = require("./../controllers/vehicleRegisterController")
const feedbackController = require("./../controllers/feedbackController")
const router = express.Router()
const VehicleRegister = require("./../models/vehicleRegisterModels")
const User = require("./../models/userModels")
const notification = require("../models/notificationModels");
const BASE_URL =process.env.BASE_URL
// const mongoose = require('mongoose');


const authenticateJWT = (req, res, next) => {
  const token = req.header('Authorization');

  if (!token) {
    return res.status(401).json({ message: 'Unauthorized' });
  }

  try {
    const decoded = jwt.verify(token.split(' ')[1], 'autochain14171234567890123456789');
    req.user = decoded;
    next();
  } catch (error) {
    return res.status(403).json({ message: 'Invalid token' });
  }
};

router.post('/signup', authController.signup)
router.post('/login', authController.login)
router.get('/logout', authController.logout)

router.get('/me', authenticateJWT, async (req, res) => {
  try {
    // The req.user.id is now accessible here
    const userinfo = await User.findById(req.user.id);
    // Fetch the user data using the user ID
    // ...
    res.status(200).json({ userinfo });
  } catch (error) {
    res.status(500).json({ message: "Error retrieving user data" });
  }
});

// registerVehicles
router.post("/upload", vehicleRegisterController.uploadVehiclePhoto)
router.post("/register", vehicleRegisterController.uploadVehiclePhoto, vehicleRegisterController.addVehicles)
router.get("/getVehicles", vehicleRegisterController.getvehicles)
router.get("/getAllVehicles", vehicleRegisterController.getAllVehicles)
router.patch("/updateVehicle", vehicleRegisterController.updateVehicles)
router.delete("/deleteVehicles", vehicleRegisterController.deleteVehicles)


// put vehicle in vehicle array in user model
// router.put('/:userId', async (req, res) => {

// GET /api/v1/users/getSpecificVehicles
router.get('/getVehiclesFromUser',authenticateJWT, async (req, res) => {
  try {


    // Find the user by ID
    const user = await User.findById(req.user.id);
    // Extract the vehicleIds from the vehicles array
    const vehicleIds = user.vehicle.map(vehicle => String(vehicle._id))
    // Return the vehicleIds as the response
    res.json(vehicleIds);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
});

// create feedback
router.post("/feedback", feedbackController.addfeedback)
router.get("/getAllFeedbacks", feedbackController.getAllFeedbacks)
router.get("/getFeedback", feedbackController.getfeedback)
router.delete("/deleteFeedback", feedbackController.deletefeedback)


// MyVehicles
router.get("/getSpecificVehicles", authenticateJWT, async (req, res) => {
  try {
    const userId = req.user.id; // Assuming the user ID is available in the req.user object

    // Fetch vehicles from the database that belong to the current user
    const vehicles = await VehicleRegister.find({ userId: userId });

    // Map the vehicles and include the imageUrl field
    const vehiclesWithImageUrl = vehicles.map(vehicle => ({
      _id: vehicle._id,
      imageUrl: `${BASE_URL}/img/registeredVehicle/${vehicle.image}`, // Replace "http://localhost:4001" with your server's hostname and image path
      vehicletype: vehicle.vehicletype,
      vehiclemodel: vehicle.vehiclemodel,
      vehiclestatus: vehicle.vehiclestatus
    }));

    res.status(200).json(vehiclesWithImageUrl);
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Server Error" });
  }
});

// MYprofilepicture

router.get("/getSpecificUsers", authenticateJWT, async (req, res) => {
  try {
    const userId = req.user.id; // Assuming the user ID is available in the req.user object

    // Find the user from the database based on the userId
    const user = await User.findOne({ _id: userId });

    if (!user) {
      return res.status(404).json({ message: "User not found" });
    }

    // Retrieve the image URL from the user object
    const imageUrl = `${BASE_URL}/img/users/${user.image}`; // Replace "http://localhost:4001" with your server's hostname and image path

    res.status(200).json({ imageUrl });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Server Error" });
  }
});

// getUserWithSpecificvehicles in myVehicle page
router.get("/getUserWithVehicles",vehicleRegisterController.getUserWithVehicles);


// My details

router.get('/vehicles/:id', async (req, res) => {
  try {
    const vehicleId = req.params.id;
    // Fetch the vehicle data using the vehicle ID
    const vehicle = await VehicleRegister.findById(vehicleId);
    if (!vehicle) {
      return res.status(404).json({ error: 'Vehicle not found' });
    }

    const imageUrl = `${BASE_URL}/img/registeredVehicle/${vehicle.image}`; // Update the image URL based on your server configuration

    // Include the imageUrl property in the response
    const vehicleData = {
      _id: vehicle._id,
      imageUrl: imageUrl,
      vehicletype: vehicle.vehicletype,
      vehiclemodel: vehicle.vehiclemodel,
      vehiclestatus: vehicle.vehiclestatus
      // Include any other properties you want to include
    };

    res.status(200).json(vehicleData);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Server Error' });
  }
});
router.put("/updateMyPassword", authController.protect, authController.updatePassword);
router.put("/updateMe", authController.protect, userController.uploadUserPhoto, userController.updateMe);

router
  .route('/')
  .get(userController.getAllUsers)
  .post(userController.createUser)
router.get("getUsers",userController.getAllUsers)

  // .route('/:id')
  // .get(userController.getUser)
  // .delete(userController.deleteUser)

router.post("/logout", authController.logout);


router.post("/postNotifications", async (req, res) => {
  try{

      const newNotification =await notification.create(req.body)
      res.status(201).json({
          status: "success", data: {
              Notification: newNotification,
          },
      })
  }catch(err){
      res.status(400).json({
          status: "fail", message: err,
      })
  }
}); 

// GET all notifications
router.get('/getNotifications', async (req, res) => {
  try {
    // Retrieve all notifications from the database
    const notifications = await notification.find();

    res.status(200).json({
      status: 'success',
      data: {
        notifications
      }
    });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Server Error' });
  }
});


// router.put("/${userId}", userController.updateUserStatus);



module.exports = router


