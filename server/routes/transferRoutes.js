const express = require('express');
const router = express.Router();
const User = require("../models/userModels")



// Define the route handler for pushing the vehicleId to the user's vehicle field
router.post('/pushVehicleId', async (req, res) => {
  const { userId, vehicleId } = req.body;

  try {
    // Update the user document in the database
    const result = await User.updateOne(
      { _id: userId },
      { $push: { vehicle: vehicleId } }
    );

    if (result.nModified === 0) {
      return res.status(404).json({ message: 'User not found' });
    }

    res.status(200).json({ message: 'VehicleId added successfully' });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Server error' });
  }
});




module.exports = router