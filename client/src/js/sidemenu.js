import React from "react";
import {
  AppstoreOutlined,
  SwapOutlined,
  MessageOutlined,
  LogoutOutlined,
} from "@ant-design/icons";
import { Menu } from "antd";
import { Outlet } from "react-router-dom";
import { useEffect, useState } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import "../css/dashboard.css"

function SideMenu() {
  const location = useLocation();
  const [selectedKeys, setSelectedKeys] = useState("/");

  useEffect(() => {
    const pathName = location.pathname;
    setSelectedKeys(pathName);
  }, [location.pathname]);

  const navigate = useNavigate();
  return (
    <div className="dashboard-container">
    <>
    
    <div className="SideMenu">
    <h3 className="admin-name">Admin</h3>
      <Menu
        className="SideMenuVertical"
        mode="vertical"
        onClick={(item) => {
          //item.key
          navigate(item.key);
        }}
        selectedKeys={[selectedKeys]}
        items={[
          {
            label: "Dashboard",
            icon: <AppstoreOutlined />,
            key: "/db/dashboard",
          },
          {
            label: "Vehicle Transferred",
            key: "/db/vehicletransfer",
            icon: <SwapOutlined />,
          },
          {
            label: "Feedback",
            key: "/db/feedback",
            icon: <MessageOutlined />,
          },
          {
            label: "Logout",
            key: "/",
            icon: <LogoutOutlined />,
          },
        ]}
      ></Menu>
    </div>
    <div  className="page-content">
      <Outlet />
      </div>
    </>
    </div>
  );
}
export default SideMenu;
