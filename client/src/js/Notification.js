import "./Notification.css";
import AutoChain from "../Images/AutoChain.png";
import { FaArrowCircleLeft, FaBell } from "react-icons/fa";
import vehicle from "../Images/vehicle.png";
import "bootstrap/dist/css/bootstrap.css";

export const Vehicleregistration = () => {
  return (
    <div>
      <div className="headingTop">
        <div id="Goback">
          <a href="#">
            <FaArrowCircleLeft />
          </a>
        </div>

        <img src={AutoChain} id="AutoChain" alt="AutoChain Logo" />

        <div id="notificon">
          <a href="#">
            <FaBell />
          </a>
        </div>

        <img
          src={require("../Images/Profile.jpg")}
          alt="ProfileIcon"
          id="profileIcon"
        />
      </div>
      <div className="container">
        <div className="row">
          <div className=" col-12 heading1">
            <h2>TRANSFER DETAILS</h2>
          </div>
        </div>
      </div>

      <div className="container">
        <div className="row one">
          <div className="col-md-6">
            <div className="vehicle">
              <img src={vehicle} alt="Vehicle Image" />
            </div>
          </div>
          <div className="col-md-6">
            <div className="detail">
              <div className="del">
                <h1>Vehicle Details</h1>
                <p className="p1">Vehicle Registration Number</p>
                <p className="p2">BP-1-A1111</p>
                <hr></hr>
                <p className="p1">Vehicle Maker</p>
                <p className="p2">Nissan</p>
                <hr></hr>
                <p className="p1">Vehicle Model</p>
                <p className="p2">Altima</p>
                <hr></hr>
                <p className="p1">Vehicle Type</p>
                <p className="p2">Light Vehicle</p>
                <hr></hr>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="container" style={{ marginTop: "3%" }}>
        <div className="row two">
          <div className="col-md-12">
            <div className="card-body">
              <h1>New Owner's Details</h1>
              <p className="p3">Full Name</p>
              <p className="p4">Dechen Namgay</p>
              <hr></hr>
              <p className="p3">Mobile Number</p>
              <p className="p4">17811042</p>
              <hr></hr>
              <p className="p3">CID Number</p>
              <p className="p4">11807000780</p>
              <hr></hr>
              <div className="vertical-center">
                <button className="button">ACCEPT</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Vehicleregistration;
