import React from "react";
import "../css/registration.css";
import { useState, useEffect } from "react";
import axios from "axios";
import Footer from '../js/footer';
// import Pop from './pop';


export const Vehicleregistration = () => {
  const [userId, setUserId] = useState("");
  const [CIDNumber, setCIDNumber] = useState('');
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [phoneNumber, setPhoneNumber] = useState('');
  const [vehicleType, setVehicleType] = useState('');
  const [vehicleModel, setVehicleModel] = useState('');
  const [vehicleColor, setVehicleColor] = useState('');
  const [vehiclePhoto, setVehiclePhoto] = useState(null);
  const [numberPlate, setNumberPlate] = useState('');

  // //POP OUT
  // const [showPopup, setShowPopup] = useState(false);

  const generateNumberPlate = () => {
    // const randomLetters = Math.random().toString(36).substr(2, 2).toUpperCase();
    const randomNumbers = Math.floor(1 + Math.random() * 90);

    const generatedPlate = `${"BP"} -${randomNumbers}-${randomNumbers}`;

    setNumberPlate(generatedPlate);
  };

  const fetchUserData = async () => {
    try {
      const storedToken = localStorage.getItem("token");
      if (storedToken) {
        const response = await axios.get("https://autochain.onrender.com/api/v1/users/me", {
          headers: {
            Authorization: `Bearer ${storedToken}`,
          },
        });

        const userData = response.data.userinfo;
        setUserId(userData._id);
        setCIDNumber(userData.cid);
        setName(userData.name);
        setEmail(userData.email);
        setPhoneNumber(userData.phoneNumber);
      } else {

      }
    } catch (response) {
      // console.error(error);
      // Handle error response
      console.log(response.data);
    }
  };

  useEffect(() => {
    // Fetch user data based on authentication or session information
    // Call the fetchUserData function
    fetchUserData();
  }, []);

  const handleSubmit = async (e) => {
    e.preventDefault();

    // Create a new FormData object
    const formData = new FormData();

    // Append the vehicle photo file to the formData
    formData.append("image", vehiclePhoto);
    formData.append("vehiclenumber", numberPlate);
    formData.append("vehicletype", vehicleType);
    formData.append("vehiclemodel", vehicleModel);
    formData.append("vehiclecolor", vehicleColor);
    console.log("userId", userId);
    formData.append("userId", userId);

    try {
      const response = await axios.post(
        "https://autochain.onrender.com/api/v1/users/register",
        formData
      );

      console.log(response.data);
      window.alert("Vehicle Registered successfully");



      // Reset the form fields
      setVehiclePhoto(null);
      setNumberPlate("");
      setCIDNumber("");
      setName("");
      setEmail("");
      setPhoneNumber("");
      setVehicleType("");
      setVehicleModel("");
      setVehicleColor("");
    } catch (error) {
      console.log(error.response.data);
      window.alert("Vehicle Registration failed");
    }
  };


  return (
    <>
      <div>
        <div className="container-2-register">
          <h2 className="vehicleregistrationForm">Vehicle Registration Form</h2>
        </div>
        <hr className="Vline"/>
        <div className="formdiv">
          <form className="Regisform" onSubmit={handleSubmit}>
            {/* Rest of the form code */}

            {/* Form input fields */}

            <div className="input-field-display">
              <div className="input-firstpart">
                <div className="column-detail-container">
                  <h3>CID Number</h3>
                  <input className="default_input2"
                    type="text"
                    value={CIDNumber}
                    onChange={(e) => setCIDNumber(e.target.value)}
                    required
                    disabled
                  />
                </div>

                <div className="column-detail-container">
                  <h3>Name</h3>
                  <input className="default_input2"
                    type="text"
                    value={name}
                    onChange={(e) => setName(e.target.value)}
                    required
                    disabled
                  />
                </div>

                <div className="column-detail-container">
                  <h3>Email</h3>
                  <input className="default_input2"
                    type="email"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    required
                    disabled
                  />
                </div>

                <div className="column-detail-container">
                  <h3>Phone Number</h3>
                  <input className="default_input2"
                    type="text"
                    value={phoneNumber}
                    onChange={(e) => setPhoneNumber(e.target.value)}
                    required
                    disabled
                  />
                </div>

                <div className="column-detail-container">
                  <h3>Vehicle Photo</h3>
                  <input className="default_input3"
                    type="file"
                    id="vehiclePhoto"
                    name="vehiclePhoto"
                    onChange={(e) => setVehiclePhoto(e.target.files[0])}
                    required
                  />
                </div>
              </div>

              <div className="coloumn column2" id="col1">



                <div className="column-input-container">
                  <label htmlFor="Vnumber">Vehicle Number</label>
                  <div className="generate-btn-container">
                    <input
                      type="text"
                      id="Vnumber"
                      value={numberPlate}
                      className="vehicle-detail-input"
                      onChange={(e) => setNumberPlate(e.target.value)}
                    />
                    <button type="button" onClick={generateNumberPlate}>Generate</button>
                  </div>
                </div>

                <div className="column-input-container">
                  <label htmlFor="Vtype">Vehicle Type</label>
                  <select
                    id="Vtype"
                    className="vehicle-detail-input"
                    value={vehicleType}
                    onChange={(e) => setVehicleType(e.target.value)}
                  ><option value="">Select the Vehicle Type</option>
                    <option value="Light Vehicle">Light Vehicle </option>
                    <option value="Earth moving Equipment">Earth moving Equipment</option>
                    <option value="Heavy bus">Heavy bus</option>
                    <option value="Heavy vehicle">Heavy vehicle </option>
                    <option value="Power Tiller">Power Tiller</option>
                  </select>
                </div>

                <div className="column-input-container">
                  <label htmlFor="Vmodel">Vehicle Model</label>
                  <select
                    id="Vmodel"
                    className="vehicle-detail-input"
                    value={vehicleModel}
                    onChange={(e) => setVehicleModel(e.target.value)}
                  ><option value="">Select the Vehicle Model</option>
                    <option value="Toyota">Toyota</option>
                    <option value="Suzuki">Suzuki</option>
                    <option value="Kia">Kia</option>
                    <option value="Hyundai">Hyundai</option>

                  </select>
                </div>

                <div className="column-input-container">
                  <label htmlFor="Vcolor">Color</label>
                  <select
                    id="Vcolor"
                    className="vehicle-detail-input"
                    value={vehicleColor}
                    onChange={(e) => setVehicleColor(e.target.value)}
                  ><option value="">Select the Color</option>
                    <option value="Blue">Blue</option>
                    <option value="Green">Green</option>
                    <option value="Orange">Orange</option>
                    <option value="Yellow">Yellow</option>
                    <option value="Purple">Purple</option>
                    <option value="Brown">Brown</option>
                    <option value="Black">Black</option>
                    <option value="White">White</option>
                    <option value="Gray">Gray</option>
                    <option value="Maroon">Maroon</option>
                    <option value="Pink">Pink</option>
                    <option value="Gold">Gold</option>
                    <option value="Cyan">Cyan</option>
                    <option value="Navy Blue">Navy Blue</option>

                  </select>
                </div>
              </div>
            </div>
            <div className="bt">
              <button className="buttont" type="submit" id="submitBut">Submit</button>

              {/* {showPopup && (
                <Pop
                  message="Do you want to proceed?"
                  onYes={handleYes}
                  onNo={handleNo}
                />
              )} */}
            </div>
          </form>
        </div>
      </div>
      <Footer />
    </>
  )
}

export default Vehicleregistration;