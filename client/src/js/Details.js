import React, { useState, useEffect } from "react";
// import "../css/details.css";
import { FaArrowCircleLeft } from 'react-icons/fa';
import { Link, useParams } from "react-router-dom";
import axios from "axios";
import "../css/details.css";
// import Pop from './pop';
import Footer from '../js/footer';

export const Details = ({ account, initiateTransfer }) => {
    const { id } = useParams();
    const [vehicle, setVehicle] = useState(null);
    const [newOwnerAddress, setNewOwnerAddress] = useState('');
    const [vehicleId, setVehicleId] = useState("");

    const handlenotifications = async () => { 
    const transferData = {
        _From: account,
        vehicleId: vehicleId
      };

      // POST request to create a notification
      await axios.post('https://autochain.onrender.com/api/v1/users/postNotifications', transferData);
      console.log("Notification created successfully");
    };
      

     // Use the initiateTransfer function when needed
     function handleTransfer() {
        const newOwnerAddress = document.getElementById("newOwnerAddress").value;
        const vehicleId = document.getElementById("vehicleId").value;
      
        initiateTransfer(newOwnerAddress, vehicleId);
      };
      const handleSubmit = (e) => {
        e.preventDefault();
    
        // Call both update functions
        handlenotifications();
        handleTransfer();
      };  


    useEffect(() => {
        fetchVehicleDetails();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);
    // //POP OUT
    // const [showPopup, setShowPopup] = useState(false);

    // const handleShowPopup = () => {
    //     setShowPopup(true);
    // };

    // const handleClosePopup = () => {
    //     setShowPopup(false);
    // };

    // const handleYes = () => {
    //     console.log("Yes option clicked");
    //     // Perform any necessary action for "Yes" option
    //     handleClosePopup();
    // };

    // const handleNo = () => {
    //     console.log("No option clicked");
    //     // Perform any necessary action for "No" option
    //     handleClosePopup();
    // };

    // const style = {
    //     color: '#965A33',
    // }

    const style2 = {
        color: '#23BC44',
    }


    const fetchVehicleDetails = async () => {
        try {
            const response = await axios.get(`https://autochain.onrender.com/api/v1/users/vehicles/${id}`);
            const vehicleData = response.data;
            setVehicle(vehicleData);       
            setVehicleId(vehicleData._id);
        } catch (error) {
            console.error(error);
        }
    };

    if (!vehicle) {
        return <div>Loading...</div>;
    }

    return (
        <>
            <div>
                <div className="container">
                    <nav className="navbar">
                        <Link to="/MyVehicle" className="goback-link">
                            <FaArrowCircleLeft id="Goback" />
                        </Link>
                        {/* <div className="mini-bar">
                            <h1>Auto</h1>
                            <h1 className="heading" style={style}>Chain</h1>
                        </div> */}
                    </nav>
                </div>

                <div className="container-2-details">
                    <h3 className="top">My Details</h3>
                </div>
                <h2 className="account-details" style={{"fontSize": "25px"}} >Current Account: {account}</h2>

                <hr className="Dline" />

                <div className="Vehicle-details-car">
                    {/* Render vehicle details */}
                    <img src={vehicle.imageUrl} alt="Vehicle" />
                    <div className="details">
                        <h2>Vehicle Details</h2>
                        <p className="v">Vehicle Type</p>
                        <p className="c"> {vehicle.vehicletype}</p>
                        <hr className="d" />
                        <p className="v">Vehicle Model</p>
                        <p className="c">{vehicle.vehiclemodel}</p>
                        <hr className="d" />
                        <p className="v">Vehicle Status</p>
                        <p className="c">Active</p>
                        <hr className="d" />
                        {/* Display other details as needed */}
                    </div>

                </div>

                <div className="container-4-vehicleowner-details">
                    <div className="detail">
                        <h4>Owner Details</h4>
                        <h5>Name</h5>
                        <article>Dechen Namgay</article>
                        <h5>Permanent Address</h5>
                        <article>Babesa, Thimphu</article>
                        <h5>Present Address</h5>
                        <article>Bajo, Wangdi Phodrang</article>
                        <h5>CID</h5>
                        <article>11807000780</article>
                    </div>

                    <div className="detail">
                        <h4>Vehicle Details</h4>
                        <h5>Seating Capacity</h5>
                        <article>5</article>
                        <h5>Initial Registration</h5>
                        <article>02/06/2023</article>
                        <h5>Color</h5>
                        <article>Black</article>
                        <h5>Status</h5>
                        <article className="status" style={style2}>Active</article>
                    </div>
                </div>
                {/* <div className="container-5">
                <div className="transfervehicle">
                    <AiFillCar id='car' />
                    <p class="t">Transfer Vehicle</p>
                    <FaArrowRight id='arrow' />
                </div>
            </div> */}
                <form onSubmit={handleSubmit}>
                    <div className="transfer2">

                        <h4 className="h4">New Owner's Details</h4>
                        <div className="transfer0">
                            <div className="in-transfer">
                                <article style={{ "marginBottom": "5px" }}>Vehicle ID</article>
                                <input type="text" id="vehicleId" defaultValue={vehicleId} />

                            </div>
                            <div className="out-transfer">
                                <article style={{ "marginBottom": "5px" }}>Account Address</article>
                                <input type="text" id="newOwnerAddress" value={newOwnerAddress} onChange={(e) => setNewOwnerAddress(e.target.value)} />
                            </div>
                        </div>

                        <div className="last">
                        <button className="button" type="submit">Transfer</button>
                            {/* {showPopup && (
                            <Pop
                                message="Do you want to proceed?"
                                onYes={handleYes}
                                onNo={handleNo}
                            />
                        )} */}
                        </div>

                    </div>
                </form>


            </div>
            <Footer />
        </>
    )
}

export default Details;