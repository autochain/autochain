import React from "react";
import { Typography, Table } from "antd";

const { Title } = Typography;

function VehicleHistory() {
  const dataSource = [
    { key: "1", col1: "12340", col2: "Hachback", col3: "Maruti Suziki", col4: "Maruti Suziki", col5: "Query History" },
    { key: "2", col1: "14324", col2: "light Vehicle", col3: "Maruti Suziki", col4: "Maruti Suziki", col5: "Query History" },
    // Add more rows as needed
  ];

  const columns = [
    {
      title: "License Number",
      dataIndex: "col1",
      key: "col1",
    },
    {
      title: "Vehicle Type",
      dataIndex: "col2",
      key: "col2",
    },
    {
      title: "Vehicle Make",
      dataIndex: "col3",
      key: "col3",
    },
    {
      title: "Vehicle Model",
      dataIndex: "col4",
      key: "col4",
    },
    {
      title: "Action",
      dataIndex: "col5",
      key: "col5",
    },
  ];

  return (
    <div>
      <Title level={3}>Vehicle History</Title>
      <Table dataSource={dataSource} columns={columns} />
    </div>
  );
}

export default VehicleHistory;


