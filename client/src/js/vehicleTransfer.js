import React from "react";
import { Typography, Table } from "antd";

const { Title } = Typography;

function VehicleTransferred({ transferHistory }) {
  const columns = [
    {
      title: "Previous Owner Full Name",
      dataIndex: "from",
      key: "from",
    },
    {
      title: "New Owner Full Name",
      dataIndex: "to",
      key: "to",
    },
    {
      title: "Vehicle ID",
      dataIndex: "vehicleID",
      key: "vehicleID",
    },
    {
      title: "Confirmed",
      dataIndex: "confirmed",
      key: "confirmed",
      render: (confirmed) => (confirmed ? "Yes" : "No"),
    },
  ];

  return (
    <div>
      <Title level={3}>Vehicle Transferred</Title>
      <Table dataSource={transferHistory} columns={columns} />
    </div>
  );
}

export default VehicleTransferred;