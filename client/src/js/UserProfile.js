import React, { useState, useEffect } from "react";
import "../css/userProfile.css";
import { Link } from "react-router-dom";
import { FaArrowCircleLeft } from 'react-icons/fa';
import axios from 'axios';
import Footer from '../js/footer';

const UserProfile = () => {
  const style = {
    color: '#965A33',
  };
  const [addPicture, setAddPicture] = useState("");
  const [profilePicture, setProfilePicture] = useState("");
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [phoneNumber, setPhoneNumber] = useState("");
  const [passwordCurrent, setCurrentPassword] = useState("");
  const [password, setNewPassword] = useState("");
  const [passwordConfirm, setPasswordConfirm] = useState("");



  const fetchUserImage = async () => {
    try {
      const storedToken = localStorage.getItem('token');
      if (storedToken) {
        const response = await axios.get('https://autochain.onrender.com/api/v1/users/getSpecificUsers', {
          headers: {
            Authorization: `Bearer ${storedToken}`,
          },
        });
        const { imageUrl } = response.data;
        setProfilePicture(imageUrl);
      } else {
        // Handle case when no stored token is found
      }
    } catch (error) {
      console.error(error);
    }
  };
  useEffect(() => {
    fetchUserImage();
  }, []);

  const fetchUserData = async () => {
    try {
      const storedToken = localStorage.getItem("token");
      if (storedToken) {
        const response = await axios.get("https://autochain.onrender.com/api/v1/users/me", {
          headers: {
            Authorization: `Bearer ${storedToken}`,
          },
        });


        const userData = response.data.userinfo;
        setName(userData.name);
        setEmail(userData.email);
        setPhoneNumber(userData.phoneNumber);
      } else {

      }
    } catch (error) {
      console.error(error);
      // Handle error response
    }
  };

  useEffect(() => {
    fetchUserData();
  }, []);


  const handleUpdateProfile = async () => {
    const formData = new FormData();
    formData.append("image", addPicture);

    try {
      const storedToken = localStorage.getItem("token");
      if (storedToken) {
        const response = await axios.put(
          "https://autochain.onrender.com/api/v1/users/updateMe",
          formData,
          {
            headers: {
              Authorization: `Bearer ${storedToken}`,
            },
          }
        );
        console.log(response.data); // Handle response accordingly
        window.alert("Profile picure updated successfully");
      } else {
        // Handle case when no stored token is found
      }
    } catch (error) {
      console.error(error);
      // Handle error response
      window.alert("Update failed");
    }

    // Reset the form fields
    setAddPicture("");
  };


  const handleUpdatePassword = async () => {
    try {
      // Send the PUT request to updateMyPassword endpoint
      const storedToken = localStorage.getItem("token");
      if (storedToken) {
        const response = await axios.put("https://autochain.onrender.com/api/v1/users/updateMyPassword", {
          passwordCurrent,
          password,
          passwordConfirm,
        }, {
          headers: {
            Authorization: `Bearer ${storedToken}`,
          },
        });
        console.log(response.data); // Handle response accordingly
        window.alert("Password update successfully");
      } else {
        // Handle case when no stored token is found
      }
    } catch (error) {
      console.error(error);
      window.alert("Password update failed");
    }
    setPasswordConfirm("");
    setCurrentPassword("");
    setNewPassword("");
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    // Call both update functions
    handleUpdateProfile();
    handleUpdatePassword();
  };

  return (
    <>
    <div>
      <div className="container">
        <nav className="navbar">
          <Link to="/home" className="goback-link">
            <FaArrowCircleLeft id="Goback" />
          </Link>
          <div className="mini-bar">
            <h1>Auto</h1>
            <h1 className="heading" style={style}>Chain</h1>
          </div>
        </nav>
      </div>
      <form onSubmit={handleSubmit}>
        <div className="formdiv">
          <h2>Edit Profile</h2>
          <div className="profile-section">
            <div className="profile-picture-section">
              <div className="profile-picture">
                <img src={profilePicture} alt="profile-12" className="profilepicture-img"></img>
              </div>
              <input
                type="file"
                id="fileInput"
                className="file-input"
                onChange={(e) => setAddPicture(e.target.files[0])}
              />
            </div>
            <div className="profile-info-section">
              <h3 style={{ padding: "10px" }}>Account Information</h3>
              <div className="info-item">
                <label>Full Name</label>
                <input  className="file-input" type="text" value={name} onChange={(e) => setName(e.target.value)} required
                  disabled />
              </div>
              <div className="info-item">
                <label>Email Address</label>
                <input  className="file-input" type="email" value={email} onChange={(e) => setEmail(e.target.value)} required
                  disabled />
              </div>
              <div className="info-item">
                <label>Mobile Phone Number</label>
                <input  className="file-input" type="tel" value={phoneNumber} onChange={(e) => setPhoneNumber(e.target.value)} required
                  disabled />
              </div>

              <h3 style={{ padding: "10px" }}>Change Password</h3>
              <div className="info-item">
                <label>Current Password</label>
                <input  className="file-input" type="password" value={passwordCurrent} onChange={(e) => setCurrentPassword(e.target.value)} />
              </div>
              <div className="info-item">
                <label>New Password</label>
                <input  className="file-input" type="password" value={password} onChange={(e) => setNewPassword(e.target.value)} />
              </div>
              <div className="info-item">
                <label>Re-enter Password</label>
                <input  className="file-input" type="password" value={passwordConfirm} onChange={(e) => setPasswordConfirm(e.target.value)} />
              </div>
            </div>
          </div>
          <button type="submit" className="save-button">Save</button>
        </div>
      </form>
    </div>
    <Footer/>
    </>
  );
};

export default UserProfile;
