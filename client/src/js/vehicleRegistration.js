import React from "react";
// import { Typography } from "antd";
import { Typography, Table} from "antd";
import { useState, useEffect } from "react";
import axios from "axios";
const { Title } = Typography;



function VehicleRegistered() {
  const [vehicles, setVehicles] = useState([]);


  useEffect(() => {
    fetchVehicles();
  }, []);

  const fetchVehicles = async () => {
    try {
      const response = await axios.get(
        "https://autochain.onrender.com/api/v1/users/getAllVehicles"
      );
      const vehicleData = response.data.data.vehicleRegister1;
      setVehicles(vehicleData);
    } catch (error) {
      throw new Error(error.message);
    }
  };

  // const updateVehicleStatus = async (vehicleId, active) => {
  //   try {
  //     // Make an API call to update the vehicle status
  //     const response = await axios.put(`http://localhost:4001/api/v1/vehicles/${vehicleId}/status`, {
  //       active: active,
  //     });
  //     console.log(response.data); // Log the API response

  //     // Update the vehicles state to reflect the updated status
  //     const updatedVehicles = vehicles.map((vehicle) => {
  //       if (vehicle._id === vehicleId) {
  //         return { ...vehicle, active: active };
  //       } else {
  //         return vehicle;
  //       }
  //     });
  //     setVehicles(updatedVehicles);
  //   } catch (error) {
  //     throw new Error(error.message);
  //   }
  // };

  const columns = [
    {
      title: "UserId",
      dataIndex: "userId",
      key: "userId",
      render: (text, record) => <span>{record.userId._id}</span>,
    },
    {
      title: "Vehicle Number",
      dataIndex: "vehiclenumber",
      key: "vehiclenumber",
    },
    {
      title: "Vehicle Type",
      dataIndex: "vehicletype",
      key: "vehicletype",
    },
    {
      title: "Vehicle Model",
      dataIndex: "vehiclemodel",
      key: "vehicle model",
    },
    {
      title: "Vehicle Color",
      dataIndex: "vehiclecolor",
      key: "vehiclecolor",
    },
    {
      title: "Vehicle Image",
      dataIndex: "image",
      key: "image",
    },
    {
      title: "Action",
      dataIndex: "col8",
      key: "col8",
      // render: (text, record) => (
      //   <Button
      //     onClick={() => updateVehicleStatus(record._id, !record.active)}
      //   >
      //     {record.active ? "Deactivate" : "Activate"}
      //   </Button>
      // ),
    },
  ];

  return (
    <div>
      <Title level={3}>Vehicle Registered</Title>
      <Table dataSource={vehicles} columns={columns} rowKey="_id" />
    </div>
  );
}

export default VehicleRegistered;


