import React, { useEffect, useRef, useState } from 'react';
import { Link, Outlet } from 'react-router-dom';
import '../css/header.css';
import axios from 'axios';
import { FaChevronDown, FaBell } from 'react-icons/fa';
const Header = ({ isLoggedIn, confirmTransfer }) => {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const headerRef = useRef(null);
  const [isDropdownOpen, setIsDropdownOpen] = useState(false);
  const [userImageUrl, setUserImageUrl] = useState('');
  const[transferId,setTransferId] = useState("");
  const[vehicleId,setVehicleId] = useState("");
  const[address, setAddress]= useState("");

  
  const handleSubmit = (e) => {
    e.preventDefault();

    handleConfirm();
    handleTransfer();
  };  

     // Use the initiateTransfer function when needed
     function handleConfirm() {
      const transferId = document.getElementById("transferId").value;
    
      confirmTransfer(transferId);
    };


    function handleTransfer(){

      const storedToken = localStorage.getItem('token');
      if (storedToken) {
        axios.get('https://autochain.onrender.com/api/v1/users/me', {
          headers: {
            Authorization: `Bearer ${storedToken}`,
          },
        })
        .then(response => {
          const userData = response.data.userinfo;
          console.log(userData);
          // Push the new vehicleId to the vehicle array on the backend
          axios.post('https://autochain.onrender.com/api/v1/users/pushVehicleId', {
            userId: userData._id,
            vehicleId: vehicleId
          })
          .then(() => {
            // Update the UI or perform any other necessary actions
            console.log('User document updated successfully');
          })
          .catch(error => {
            console.error(error);
            // Handle error response from the server
          });
        })
        .catch(error => {
          console.error(error);
          // Handle error response from the server
        });
      }
    };


  const toggleModal = () => {
    setIsModalOpen(!isModalOpen);
  };
  useEffect(() => {
    fetchUserData();
  }, []);

  useEffect(() => {
    const header = headerRef.current;
    const headerHeight = header.offsetHeight; // Get the height of the header

    const onScroll = () => {
      if (window.scrollY > headerHeight) {
        header.classList.add('visible');
      } else {
        header.classList.remove('visible');
      }
    };

    window.addEventListener('scroll', onScroll);

    return () => {
      window.removeEventListener('scroll', onScroll);
    };
  }, []);

  const toggleDropdown = () => {
    setIsDropdownOpen(!isDropdownOpen);
  };

  const handleLogout = async () => {
    try {
      // Clear the token from local storage
      localStorage.removeItem('token');
      localStorage.removeItem('isLoggedIn');

      window.location.href = '/';
    } catch (error) {
      console.error(error);
      // Handle error response from the server
    }
  };

  const handleUpdateProfile = () => {
    // Handle update profile logic
    window.location.href = '/profile';

  }
  const fetchUserData = async () => {
    try {
      const storedToken = localStorage.getItem('token');
      if (storedToken) {
        const response = await axios.get('https://autochain.onrender.com/api/v1/users/getSpecificUsers', {
          headers: {
            Authorization: `Bearer ${storedToken}`,
          },
        });
        const { imageUrl } = response.data;
        setUserImageUrl(imageUrl);
        // setUser
      } else {
        // Handle case when no stored token is found
      }
    } catch (error) {
      console.error(error);
    }
  };
  const fetchNotifications = async () => {
    try {
      const response = await axios.get("https://autochain.onrender.com/api/v1/users/getNotifications");
      
      // Handle the response and update your state or perform any necessary actions
      const { data } = response.data;

      const notifications = data;
      setAddress(notifications.notifications[0]._From);
      setVehicleId(notifications.notifications[0].vehicleId);
      // Update your state or perform any necessary actions with the notifications data
      // setNotifications(notifications);
    } catch (error) {
      console.error(error);
      // Handle error response from the server
    }
  };
  useEffect(() => {
    fetchNotifications();
  }, []);

  const renderLoggedOutNavigation = () => {

    return (

      <ul className="nav-links">
        <Link to="/" className="logo logos">
          Auto Chain
        </Link>
        <li>
          <a href="#about">About</a>
        </li>
        <li>
          <a href="#services">Services</a>
        </li>
        <li>
          <a href="#contact">Contact</a>
        </li>
      </ul>
    );
  };

  const renderLoggedInNavigation = () => {
    return (
      <ul className="nav-links">
        <Link to="/home" className="logo">
          Auto Chain
        </Link>
        <li>
          <Link to="/home">Home</Link>
        </li>
        <li>
          <Link to="/MyVehicle">MyVehicle</Link>
        </li>
        <li>
          <Link to="/register">Register</Link>
        </li>

        <li className="dropdown">
          <div className='profilepicure-notifi'>
            <span className='notification-icon1' onClick={toggleModal}>
              <FaBell className="notification-icon2" /> {/* Notification Icon */}
            </span>
            <div className='profile-dropdown'>
              <img
                src={userImageUrl}
                alt="Profile"
                className="user-picture"
                onClick={toggleDropdown}
              />
              <span className='dropdown-icon'>
                <FaChevronDown /> {/* Dropdown Icon */}
              </span>
            </div>
          </div>
          {isDropdownOpen && (
            <ul className="dropdown-menu">
              <li onClick={handleUpdateProfile}>Update Profile</li>
              <li onClick={handleLogout}>Logout</li>
            </ul>
          )}
        </li>
      </ul>
    );
  };



  return (
    <>
      <div className='header-container'>
        <header ref={headerRef} className={`header ${isLoggedIn ? 'logged-in' : ''}`}>
          <nav>
            {isLoggedIn ? renderLoggedInNavigation() : renderLoggedOutNavigation()}
          </nav>

        </header>
      </div>
      <Outlet />


      {/* Modal */}
      {isModalOpen && (
       
        <div className="modal-notification">
          {/* Modal content */}
          <form onSubmit={handleSubmit}>
          <div className="modal-content-notification">
            <h2>Notifications</h2>
            {/* <p>From Account Address: {_from}</p> */}
            <p>From Account Address: {address}</p>
            {/* <p>Vehicle ID: {vehicleId}</p> */}
            <p>Vehicle ID: {vehicleId} </p>

            {/* Add the contents here */}
            <div className="notification-input">
            <article style={{ "marginBottom": "5px" }}>Account Address</article>
            <input type="text"className='transferId' id="transferId" value={transferId} onChange={(e) => setTransferId(e.target.value)} />
            </div>
            {/* <button onClick={confirmTransfer}>Confirm</button> */}
            <button className='confirm-button' type='submit'>Confirm</button>
          </div>
          </form>
          {/* Modal close button */}
          <button className="modal-close-notification" onClick={toggleModal}>
            Close
          </button>
        </div>
  
      )}
    </>
  );
};
export default Header;
