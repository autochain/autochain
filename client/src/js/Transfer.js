import React from "react";
import i1 from "../img/i1.png";
import Footer from '../js/footer';
import '../css/transfer.css';


export const Transfer = () => {

    return (
        <>
        <div>
            <div class="container-2-transfer">
                <h3>Vehicle Ownership Transfer</h3>
            </div>

            <hr />

            <div class="container-3">
                <img src={i1} alt="#" />
                <div class="detail">
                    <h4>Vehicle Details</h4>
                    <p class="h">Vehicle Registration Number.</p>
                    <p>BP-1-A7011</p>
                    <hr class="g" />
                    <p class="h">Vehicle Make </p>
                    <p> Nissan</p>
                    <hr class="g" />
                    <p class="h">Vehicle Model </p>
                    <p>Altima</p>
                    <hr class="g" />
                    <p class="h">Vehicle Type</p>
                    <p>Light Vehicle</p>
                </div>
            </div>

            <div class="container-4">
                <div class="in-container-4">
                    <h4>New Owner's Details</h4>
                    <article>Full Name</article>
                    <input></input>
                    <article>Mobile Number</article>
                    <input></input>
                    <article>CID Number</article>
                    <input></input>
                </div>
                <button>Transfer</button>
            </div>


        </div>
        <Footer/>
        </>
    )
}

export default Transfer;