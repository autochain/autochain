import React from "react";
import "../css/about.css";
import { Link } from 'react-router-dom';
// import Carousel from 'react-bootstrap/Carousel';
import 'bootstrap/dist/css/bootstrap.min.css';
import myimage0 from "../img/carphoto.png";
import myimage1 from "../img/indexcar1.png";
import myimage2 from "../img/applytransfer.png";
import myimage3 from "../img/applyregister.png";
import myimage4 from "../img/certificate.png"
import myimage5 from "../img/handshake.png"
import myimage6 from "../img/check.png"
import myimage7 from "../img/call.png"
import myimage8 from "../img/Email.png"
import myimage9 from "../img/Location.png"
import myimage10 from "../img/dechen.png"
import myimage11 from "../img/bijay.png"
import myimage12 from "../img/Paika.png"
import myimage13 from "../img/kuenza.png"
import Footer from '../js/footer';




const IndexPage = () => {
  return (
    <>
    <div>
      <div className="FirstSection" >
        <div className="container my-2">
          <div className="row align-items-center">
            <div className="col-md-6 ml-5">
              <h2 style={{ fontSize: "70px" }}><span style={{ color: "brown" }}>Auto</span> Chain</h2>
              <p>Vehicle Ownership Transfer</p>
              <Link to="login" className="btn btn-dark">LOGIN</Link>
            </div>
            <div className="col-md-5" style={{ marginLeft: "60px" }}>
              <img src={myimage0} className="img-fluid" alt="carphoto" />
            </div>
          </div>
        </div>
        <div>
        </div>
        <h1 className="aboutUs"id="about">About Us</h1>
        <h2 className="welcome">WELCOME TO AUTOCHAIN</h2>
      </div>
      <div className="section1">
        <div className="column-image">
          <img src={myimage1} alt="car" />
        </div>
        <div className="column-description">
          <h2>About Autochain</h2>
          <div className="about-line"></div>
          <p>
            Take control of your vehicle's registration and ownership transfers
            with blockchain. Register or transfer ownership of your vehicle
            securely and conveniently - start now!
          </p>
          <p>
            Need help with registering your vehicle or transferring ownership?
            Let us guide you through the process with the power of blockchain
            technology. Contact us now and experience seamless registration and
            ownership transfers
          </p>
        </div>
      </div>
      <div className="section2" id="services">
        <h1>Our Services</h1>
        <div className="section2_inside1">
          <div className="column">
            <img src={myimage2} alt="icon1" />
            <Link to="/login" style={{color:"black"}}><h3>Apply Ownership Transfer</h3></Link>
            <p>
              Streamline ownership transfer for your vehicle using blockchain.
              Apply now and experience a fast and secure transfer process
            </p>
          </div>
          <div className="column">
            <img src={myimage3} alt="icon2" />
            
            <Link to="/login#about" style={{color:"black"}}><h3>Apply Vehicle Registration</h3></Link>
            <p>
              Experience hassle-free vehicle registration using blockchain.
              Apply now and register your vehicle with speed and security
            </p>
          </div>
        </div>
      </div>
      <div className="section3" >
        <h4>Requirement</h4>
        <h2>Vehicle Ownership transfer</h2>
        <div className="section3_inside">
          <div className="card1">
            <img src={myimage5} alt="Icon 1" />
            <p>Sale Aggrement</p>
          </div>
          <div className="card1">
            <img src={myimage4} alt="Icon 2" />
            <p>Document Submission</p>
          </div>
          <div className="card1">
            <img src={myimage6} alt="Icon 3" />
            <p>Clearance Certificate</p>
          </div>
        </div>
      </div>
      <div className="section4" id="contact">
        <h1 className="aboutUs">Contact Us</h1>
        <h2 className="welcome">GET IN TOUCH WITH US</h2>
        <div className="section4_inside">
          <div className="card2">
            <img src={myimage7} alt="Icon 1" />
            <p>Call Us Now</p>
          </div>
          <div className="card2">
            <img src={myimage8} alt="Icon 2" />
            <p>Mail Us Today</p>
          </div>
          <div className="card2">
            <img src={myimage9} alt="Icon 3" />
            <p>Our Location</p>
          </div>
        </div>
      </div>
      <div className="section5">
        <h2>Meet Our Team</h2>
        <div className="OurTeam-line"></div>
      </div>
      <div className="TeamMember">
        <div className="card5">
          <img src={myimage10} alt="Icon 1" />
          <p>Dechen Namgay</p>
        </div>
        <div className="card5">
          <img src={myimage11} alt="Icon 1" />
          <p>Bijay Kumar</p>
        </div>
        <div className="card5">
          <img src={myimage13} alt="Icon 1" />
          <p>Kuenza Lhaden</p>
        </div>
        <div className="card5">
          <img src={myimage12} alt="Icon 1" />
          <p>Sonam Tshering</p>
        </div>
         {/* <div classNameName="card5">
          <img src={myimage10} alt="Icon 1" />
          <p>Tashi Wangdi</p>
        </div> */}
      </div>
      {/* <Carousel style={{ height: '100%' }}>
            <Carousel.Item interval={1000}>
              <img
                classNameName="d-block w-100 h-50"
                src={myimage11}
                alt="First slide"
              />
              <Carousel.Caption>
                <h3>First slide label</h3>
                <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
              </Carousel.Caption>
            </Carousel.Item>
            <Carousel.Item interval={500}>
              <img
                classNameName="d-block w-100 h-50"
                src={myimage10}
                alt="Second slide"
              />
              <Carousel.Caption>
              </Carousel.Caption>
            </Carousel.Item>
            <Carousel.Item>
              <img
                classNameName="d-block w-100 h-50"
                src={myimage13}
                alt="Third slide"
              />
              <Carousel.Caption>
                <h3>Third slide label</h3>
                <p>
                  Praesent commodo cursus magna, vel scelerisque nisl consectetur.
                </p>
              </Carousel.Caption>
            </Carousel.Item>
          </Carousel> */}

    </div>
    <Footer/>
    </>
  );
};

export default IndexPage;
