import React from "react";
import { useState } from "react";
import axios from "axios";
import FormInput from "./FormInput";

const Login = ({setIsLoggedIn}) => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const inputs = [
    {
      id: "email",
      name: "email",
      type: "email",
      placeholder: "Email",
      errorMessage: "It should be a valid email address!",
      label: "Email",
      required: true,
    },
    {
      id: "password",
      name: "password",
      type: "password",
      placeholder: "Password",
      errorMessage:
        "Password should be 8-20 characters and include at least 1 letter, 1 number and 1 special character!",
      label: "Password",
      pattern: `^(?=.*[0-9])(?=.*[a-zA-Z])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,20}$`,
      required: true,
    },
  ];

  const onChange = (e) => {
    if (e.target.name === "email") {
      setEmail(e.target.value);
    } else if (e.target.name === "password") {
      setPassword(e.target.value);
    }
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const res = await axios.post("https://autochain.onrender.com/api/v1/users/login", {
        email: email,
        password: password,
      });
      if (res.data.data) {
        localStorage.setItem("token", res.data.token);
        alert("Login Successful");
        setIsLoggedIn(true); // Update the isLoggedIn state to true
        localStorage.setItem("isLoggedIn", true);
        window.location.href = "/home";
      } else {
        alert("Wrong User Credentials");
      }
    } catch (err) {
      console.log(err);
      alert("Error Logging In");
    }
  };

  return (
    <div className="app2">
      <form className="form-container" onSubmit={handleSubmit}>
        <h1>Login with your credentials</h1>
        {inputs.map((input) => (
          <FormInput
            key={input.id}
            {...input}
            value={input.name === "email" ? email : password}
            onChange={onChange}
          />
        ))}
        <div className="Cbox">
        <input type="checkbox" id="remember me"></input>
        <label className="form-check-label" htmlFor="remember me">
          Remember me
        </label>
        </div>
        <button id="loginbutton" className="loginbutton"> Login</button>
        {/* <p className="link_password">
          Forgot password ? <a href="/services" className="signupblue">Click here</a>
        </p> */}
        <p className="link_Signup">
          Dont have an account?<a href="/SignUp" className="signupblue">Sign Up</a>
        </p>
      </form>
    </div>
  );
};

export default Login;
