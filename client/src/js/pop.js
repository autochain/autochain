import React from 'react';
import '../css/pop.css';

const Pop = ({ message, onYes, onNo }) => {
    return (
        <div className="popup-container">
            <div className="popup-message">
                <p>{message}</p>
                <div className="popup-buttons">
                    <button className='yes' onClick={onYes}>Yes</button>
                    <button className='no' onClick={onNo}>No</button>
                </div>
            </div>
        </div>
    );
};

export default Pop;
