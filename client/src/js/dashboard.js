import React from "react";
import {
  CarOutlined,
  SwapOutlined,
  HistoryOutlined,
} from "@ant-design/icons";
import { useState, useEffect } from "react";
import { Card, Col, Row, Space, Statistic, Table, Typography } from "antd";
import axios from "axios";
import { Button } from "antd";

function Dashboard() {
  const [users, setUsers] = useState([]);
  const [feedbackCount, setFeedbackCount] = useState(0);
  const [vehicleCount, setVehicleCount] = useState(0);

  useEffect(() => {
    // Fetch user data, feedback count, and vehicle count from the backend API
    Promise.all([fetchUsers(), fetchFeedbackCount(), fetchVehicleCount()])
      .then(([userData, feedbackCount, vehicleCount]) => {
        setUsers(userData);
        setFeedbackCount(feedbackCount);
        setVehicleCount(vehicleCount);
      })
      .catch((error) => {
        console.error("Error fetching data:", error);
      });
  }, []);

  const fetchUsers = async () => {
    try {
      const response = await axios.get("https://autochain.onrender.com/api/v1/users/");
      const userData = response.data.data;

      // Convert the user data object to an array
      const userList = Object.keys(userData).map((key) => userData[key]);

      return userList;
    } catch (error) {
      throw new Error(error.message);
    }
  };

  const fetchFeedbackCount = async () => {
    try {
      const feedbackresponse = await axios.get("https://autochain.onrender.com/api/v1/users/getAllFeedbacks");
      const feedbacksData = feedbackresponse.data.data;

      const feedbacklist = feedbacksData.length;

      return feedbacklist
    } catch (error) {
      throw new Error(error.message);
    }
  };
  const fetchVehicleCount = async () => {
    try {
      const vehicleResponse = await axios.get("https://autochain.onrender.com/api/v1/users/getAllVehicles");
      const vehiclesData = vehicleResponse.data.data.vehicleRegister1;

      const vehicleCount = vehiclesData.length;

      return vehicleCount;
    } catch (error) {
      throw new Error(error.message);
    }
  };
  const handleStatusToggle = async (userId, active) => {
    try {
      // Make an API call to update the user's status
      const response = await axios.put(`https://autochain.onrender.com/api/v1/users/${userId}`, {
        active: active,
      });
      console.log(response.data); // Log the API response
  
      // Update the users state to reflect the updated status
      const updatedUsers = users.map((user) => {
        if (user._id === userId) {
          return { ...user, active: active };
        } else {
          return user;
        }
      });
      setUsers(updatedUsers);
    } catch (error) {
      throw new Error(error.message);
    }
  };
  const columns = [
    {
      title: "UserId",
      dataIndex: "_id",
      key: "_id",
    },
    {
      title: "Name",
      dataIndex: "name",
      key: "name",
    },
    {
      title: "CID",
      dataIndex: "cid",
      key: "cid",
    },
    {
      title: "Phone Number",
      dataIndex: "phoneNumber",
      key: "phoneNumber",
    },
    {
      title: "Email",
      dataIndex: "email",
      key: "email",
    },
    {
      title: "Status",
      dataIndex: "active",
      key: "active",
      value:"active",
      render: (text, record) => (
        <Button onClick={() => handleStatusToggle(record._id, !record.active)}>
          {record.active ? "Accept" : "Deactivate"}
        </Button>
      ),
    },
  ];


  return (
    <div>
      <Typography.Title level={4}>Dashboard</Typography.Title>
      <Row gutter={[16, 16]}>
        <Col xs={24} sm={12} md={8}>
          <DashboardCard
            icon={
              <HistoryOutlined
                style={{
                  color: "green",
                  backgroundColor: "rgba(0,255,0,0.5)",
                  borderRadius: 20,
                  fontSize: 24,
                  padding: 8,
                }}
              />
            }
            title={"Feedback"}
            value={feedbackCount}
          />
        </Col>
        <Col xs={24} sm={12} md={8}>
          <DashboardCard
            icon={
              <CarOutlined
                style={{
                  color: "purple",
                  backgroundColor: "rgba(0,255,255,0.25)",
                  borderRadius: 20,
                  fontSize: 24,
                  padding: 8,
                }}
              />
            }
            title={"Vehicle Registered"}
            value={vehicleCount}
          />
        </Col>
        <Col xs={24} sm={12} md={8}>
          <DashboardCard
            icon={
              <SwapOutlined
                style={{
                  color: "blue",
                  backgroundColor: "rgba(0,0,255,0.25)",
                  borderRadius: 20,
                  fontSize: 24,
                  padding: 8,
                }}
              />
            }
            title={"Vehicle Transferred"}
            value={5}
          />
        </Col>
      </Row>
      <div style={{ padding: "16px" }}>
        <Table dataSource={users} columns={columns} pagination={false}
          rowKey={(record) => record._id} // Provide a unique key for each table row
        />
      </div>
    </div>
  );
}

function DashboardCard({ title, value, icon }) {
  return (
    <Card className="dashboard-card">
      <div className="card-body">
        <Space direction="horizontal">
          {icon}
          <Statistic title={title} value={value} />
        </Space>
      </div>
    </Card>
  );
}

export default Dashboard;
