import React, { useState, useEffect } from "react";
import { Typography, Table } from "antd";
import axios from "axios";

const { Title } = Typography;

function VehicleFeedback() {
  const [feedbacks, setFeedbacks] = useState([]);

  useEffect(() => {
    fetchFeedbacks()
      .then((data) => {
        setFeedbacks(data);
      })
      .catch((error) => {
        console.error("Error fetching feedback data:", error);
      });
  }, []);

  const fetchFeedbacks = async () => {
    try {
      const response = await axios.get(
        "https://autochain.onrender.com/api/v1/users/getAllFeedbacks"
      );
      const feedbackList = response.data.data;
      return feedbackList;
    } catch (error) {
      throw new Error(error.message);
    }
  };

  const columns = [
    {
      title: "Name",
      dataIndex: "name",
      key: "name",
    },
    {
      title: "Email",
      dataIndex: "email",
      key: "email",
    },
    {
      title: "Date",
      dataIndex: "PostDate",
      key: "PostDate",
    },
    {
      title: "Feedback",
      dataIndex: "reason",
      key: "reason",
    },
  ];

  return (
    <div>
      <Title level={3}>Vehicle Feedback</Title>
      <Table
        dataSource={feedbacks}
        columns={columns}
        pagination={false}
        rowKey="_id" // Assuming "_id" is the unique identifier field in feedback objects
      />
    </div>
  );
}

export default VehicleFeedback;
