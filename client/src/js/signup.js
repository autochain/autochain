import React from "react";
import { useState } from "react";
import FormInput from "./FormInput";
import { useNavigate } from 'react-router-dom';

const SignUp = () => {
  // const [showMessage, setShowMessage] = useState(false);
  const [values, setValues] = useState({
    name: "",
    email: "",
    cid: "",
    phoneNumber: "",
    accountAddress:"",
    password: "",
    passwordConfirm: "",
  });

  const inputs = [
    {
      id: 1,
      name: "name",
      type: "text",
      placeholder: "Username",
      errorMessage:
        "Username should be 3-16 characters and shouldn't include any special character!",
      label: "Username",
      pattern: "^[A-Za-z0-9]{3,16}$",
      required: true,
    },
    {
      id: 2,
      name: "email",
      type: "email",
      placeholder: "Email",
      errorMessage: "It should be a valid email address!",
      label: "Email",
      required: true,
    },
    {
      id: 3,
      name: "cid",
      type: "number",
      placeholder: "CID number",
      errorMessage: "It should be a valid CID!",
      label: "CID",
      pattern: "^[0-9]{11}",
      required: true,
    },
    {
      id: 4,
      name: "phoneNumber",
      type: "number",
      placeholder: "Phone number",
      errorMessage: "It should be a valid phone number!",
      label: "Phone",
      pattern: "^(17|77)[0-9]{6}$",
      required: true,
    },
    {
      id: 5,
      name: "accountAddress",
      type: "text",
      placeholder: "Account Address",
      errorMessage: "It should be a valid Account number!",
      label: "Account Address",
      required: true,
    },
    {
      id: 6,
      name: "password",
      type: "password",
      placeholder: "Password",
      errorMessage:
        "Password should be 8-20 characters and include at least 1 letter, 1 number and 1 special character!",
      label: "Password",
      pattern: `^(?=.*[0-9])(?=.*[a-zA-Z])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,20}$`,
      required: true,
    },
    {
      id: 7,
      name: "passwordConfirm",
      type: "password",
      placeholder: "Confirm Password",
      errorMessage: "Passwords don't match!",
      label: "Confirm Password",
      pattern: values.password,
      required: true,
    },
  ];


  const navigate = useNavigate();

  const onChange = (e) => {
    setValues({ ...values, [e.target.name]: e.target.value });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    // Check if password and confirmPassword match
    if (values.password !== values.passwordConfirm) {
      console.log("Passwords don't match");
      return;
    }

    try {
      const response = await fetch("https://autochain.onrender.com/api/v1/users/signup", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(values),
      });

      const data = await response.json();
      if (data.status === "success") {
        // setShowMessage(true);
        alert("Registration Successful");
        navigate('/login');
      } else {
        alert("Registration Failed")
      }
    } catch (error) {
    }
  }
  return (
    <div className="app2">
      <form onSubmit={handleSubmit} className="form-container">
        <h1>Register a New Account</h1>
        {inputs.map((input) => (
          <FormInput
            key={input.id}
            {...input}
            value={values[input.name]}
            onChange={onChange}
          />
        ))}
        <div className="Box">
        <input className="box" type="checkbox" id="remember me"></input>
        <label className="form-check-label" htmlFor="remember me">
          Accept all the terms of use
        </label>
        </div>
        <button className="signup_button" type="submit">Register</button>

        <p className="link_password">
          Already registered?<a href="/login" className="signupblue">Login</a>
        </p>
      </form>
      {/* {showMessage && (
        <div className="message-container">
          <div className="message">
            <p>Registration successful!</p>
            <button onClick={() => setShowMessage(false)}>Close</button>
          </div>
        </div>
      )} */}
    </div>
  );
};

export default SignUp;
