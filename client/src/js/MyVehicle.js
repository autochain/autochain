
import "../css/myVehicle.css";
import { Link } from "react-router-dom";
import React, { useState, useEffect } from "react";
import axios from "axios";
import Footer from '../js/footer';

export const MyVehicle = () => {
    const [vehicles, setVehicles] = useState([]);
    // const [userId, setUserId] = useState('');
    const[newVehicles,setNewVehicles] =useState([]);

    useEffect(() => {
        fetchVehicleData();
    }, []);

    const fetchVehicleData = async () => {
        try {
            const storedToken = localStorage.getItem("token");
            if (storedToken) {
                const response = await axios.get("https://autochain.onrender.com/api/v1/users/getSpecificVehicles", {
                    headers: {
                        Authorization: `Bearer ${storedToken}`,
                    },
                });
                const vehicleData = response.data;
                setVehicles(vehicleData);
            } else {

            }

        } catch (error) {
            console.error(error);
        }
    };


    // useEffect(() => {
    //     fetchUserData();
    //   }, []);

    // const fetchUserData = async () => {
    //     try {
    //       const storedToken = localStorage.getItem("token");
    //       if (storedToken) {
    //         const response = await axios.get("http://localhost:4001/api/v1/users/me", {
    //           headers: {
    //             Authorization: `Bearer ${storedToken}`,
    //           },
    //         });
    //         const userData = response.data.userinfo;
    //         setUserId(userData._id);
    //       }
    //     } catch (error) {
    //       console.error(error);
    //     }
    //   };

      useEffect(() => {
      
             fetchVehiclesData();
              // eslint-disable-next-line react-hooks/exhaustive-deps
         }, []);
   
    const fetchVehiclesData = async () => {
        try {
          const storedToken = localStorage.getItem("token");
          if (storedToken) {
            const response = await axios.get("https://autochain.onrender.com/api/v1/users/getVehiclesFromUser", {
              headers: {
                Authorization: `Bearer ${storedToken}`
              }
            });
            const vehicleData = response.data;
            setNewVehicles(vehicleData);
          }
        } catch (error) {
          console.error(error);
        }
      };
    
 
    return (
        <>
        <div>
            <div className="container-2-myvehicle">
                <h3 className="m">My Vehicle</h3>
            </div>
            <hr className="Vlinemyvehicle"/>
            {vehicles.map((vehicle) => (
                <div key={vehicle._id} className="container-3">
                <img src={vehicle.imageUrl} alt="Vehicle" />
                <div className="contant-1">
                    <p className="p">Vehicle Type</p>
                    <p className="k">{vehicle.vehicletype}</p>
                    <hr className="b" />
                    <p className="p">Vehicle Model</p>
                    <p className="k">{vehicle.vehiclemodel}</p>
                    <hr className="b" />
                    <p className="p">Vehicle Status</p>
                    <p className="k">Active</p>
                    <hr className="b" />
                    <button className="moredetailsbutton">
                        <Link to={`/Details/${vehicle._id}`} className="more-details-link">
                            More Details
                        </Link>
                    </button>
                </div>
            </div>
            ))}


            {newVehicles.map((vehicle) => (
                <div key={vehicle} className="container-3">
                <img src={vehicle.imageUrl} alt="Vehicle" />
                <div className="contant-1">
                    <p className="p">Vehicle ID</p>
                    <p className="k">{vehicle}</p>
                    <hr className="b" />
                    <p className="p">Vehicle Type</p>
                    <p className="k">{vehicle.vehicletype}</p>
                    <hr className="b" />
                    <p className="p">Vehicle Model</p>
                    <p className="k">{vehicle.vehiclemodel}</p>
                    <hr className="b" />
                    <p className="p">Vehicle Status</p>
                    <p className="k">{vehicle.vehiclestatus}</p>
                    <hr className="b" />
                    <button className="moredetailsbutton">
                        <Link to={`/Details/${vehicle}`} className="more-details-link">
                            More Details
                        </Link>
                    </button>
                </div>
            </div>
            ))}


        </div>
            <Footer/>
        </>
    );
};

export default MyVehicle;
