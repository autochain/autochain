import React from 'react';
import { useState } from 'react';
import axios from 'axios';
import '../css/footer.css';

const Footer = () => {
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [reason, setReason] = useState('');

  const handleSubmit = (e) => {
    e.preventDefault();

    // Generate current date
    const currentDate = new Date().toISOString();

    // Create feedback object with data and date
    const feedbackData = {
      name,
      email,
      reason,
      PostDate: currentDate,
    };

    // Send the feedback data to the backend API
    axios
      .post('https://autochain.onrender.com/api/v1/users/feedback', feedbackData)
      .then((response) => {
        console.log(response.data);
        // Handle success response
        window.alert("Feedback sent")
      })
      .catch((error) => {
        console.log(error);
        // Handle error response
     
      });

    // Reset the form fields
    setName('');
    setEmail('');
    setReason('');
  };




  return (
    <footer className="footer">
      <div className="feedback">
        <h3>Send Us Feedback</h3>
        <form className="form1" onSubmit={handleSubmit}>
          <div className="form-group">
            <label htmlFor="name">Name:</label>
            <input
              type="text"
              id="name"
              name="name"
              value={name}
              onChange={(e) => setName(e.target.value)}
              required
            />
          </div>
          <div className="form-group">
            <label htmlFor="email">Email:</label>
            <input
              type="email"
              id="email"
              name="email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
              required
            />
          </div>
          <div className="form-group">
            <label htmlFor="message">Message:</label>
            <textarea
              id="message"
              name="message"
              value={reason}
              onChange={(e) => setReason(e.target.value)}
              required
            ></textarea>
          </div>
          <button className="send" type="submit">
            Send
          </button>
        </form>
      </div>
      <div className="links1">
        <h3>Menu</h3>
        <ul>
          <li><a href="/">Home</a></li>
          <li><a href="/about">About</a></li>
          <li><a href="/contact">Contact</a></li>
        </ul>
      </div>
      <div className="links2">
        <h3>Our Website</h3>
        <ul>
          <li><a href="/">Privacy and Policies</a></li>
          <li><a href="/about">Terms and Conditions</a></li>
          <li><a href="/contact">Support</a></li>
        </ul>
      </div>
    </footer>
  );
};

export default Footer;