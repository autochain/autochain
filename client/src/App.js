import React from 'react';
import Web3 from 'web3';
import { useState, useEffect } from "react";
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Header from './js/header';
import IndexPage from './js/Index';
import Home from './js/homePage';
import Login from './js/Login';
import SignUp from './js/signup';
import MyVehicle from './js/MyVehicle';
import Details from './js/Details';
import UserProfile from './js/UserProfile';
import Vehicleregistration from './js/registration';
import SideMenu from "./js/sidemenu";
import Dashboard from './js/dashboard';
// import VehicleTransferred from './js/vehicleTransfer';
import VehicleRegistered from './js/vehicleRegistration';
import VehicleFeedback from './js/feedback';
import { VEHICLE_OWNERSHIP_TRANSFER_ABI, VEHICLE_OWNERSHIP_TRANSFER_ADDRESS } from "./abi/config_VehicleOwnershipTransfer";
// import { notification } from 'antd';
const App = () => {
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const [account, setAccount] = useState('');
  const [contract, setContract] = useState(null);
  const [transferCount, setTransferCount] = useState(0);


  console.log("Blockchain Contract outside:",contract)

  useEffect(() => {
    if (!window.ethereum)
      throw new Error("No crypto wallet found. Please install it.");
    window.ethereum.send("eth_requestAccounts");
    loadBlockchainData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  
  const loadBlockchainData = async () => {

    const web3 = new Web3(Web3.givenProvider || "https://sepolia.infura.io/v3/3cd937966a054a41b206c4a07514388e");
    const accounts = await web3.eth.getAccounts();
    setAccount(accounts[0]);

    // Load the blockchain
    const vehicleContract = new web3.eth.Contract(
      VEHICLE_OWNERSHIP_TRANSFER_ABI,
      VEHICLE_OWNERSHIP_TRANSFER_ADDRESS
    );

    setContract(vehicleContract);
    const transferCounts = await vehicleContract.methods.transferCount().call();
    setTransferCount(transferCounts);

    const transferArray = [];
    for (let i = 0; i < transferCount; i++) {
      const transfer = await contract.methods.getTransferHistory(i).call();
      transferArray.push(transfer);
    }
  };

    // Get the number of records for all lists in the blockchain

 
  useEffect(() => { 
    
    // Check if the user is logged in on page load
    const storedIsLoggedIn = localStorage.getItem("isLoggedIn");
    if (storedIsLoggedIn) {
      setIsLoggedIn(true);
    }
     // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const initiateTransfer = async (newOwnerAddress, vehicleId) => {
    if (!contract) {
      console.log("Blockchain data is not available");
      return;
    }

    try {
      // Call the initiateTransfer function on the smart contract
      await contract.methods.initiateTransfer(newOwnerAddress, vehicleId).send({ from: account });
      console.log("Transfer initiated successfully");

      // Create a notification
   



      // Additional actions or display success message
    } catch (error) {
      console.log("Error initiating transfer:", error);
      // Handle error
    }
  };

  const confirmTransfer = async (transferId) => {
    if (!contract) {
      console.log("Blockchain data is not available");
      return;
    }

    try {
      // Call the confirmTransfer function on the smart contract
      await contract.methods.confirmTransfer(transferId).send({ from: account });
      console.log("Transfer confirmed successfully");

      // Additional actions or display success message
    } catch (error) {
      console.log("Error confirming transfer:", error);
      // Handle error
    }
  };




  return (
    <Router>
      <div className="app">

        <Routes>
        <Route path="login" element={<Login setIsLoggedIn={setIsLoggedIn} />} />
            <Route path="signUp" element={<SignUp />} />
            <Route path="/" element={<Header isLoggedIn={isLoggedIn} confirmTransfer={confirmTransfer} />}>
            <Route index element={<IndexPage />} />
            <Route path="home" element={<Home />} />
            <Route path="MyVehicle" element={<MyVehicle />} />
            <Route
              path="Details/:id"
              element={<Details account={account} initiateTransfer={initiateTransfer}  />}
              // render={({ match }) => (
              //   <Details
              //     account={account}
              //     initiateTransfer={initiateTransfer}
              //   />
              // )}
            />
            <Route path="profile" element={<UserProfile />} />
            <Route path="register" element={<Vehicleregistration />} />
          </Route>


          <Route path="/db" element={<SideMenu />}>
            <Route index path='dashboard' element={<Dashboard />} />
            <Route path='vehicleregister' element={<VehicleRegistered />} />
            <Route path='feedback' element={<VehicleFeedback />} />
          </Route>
        </Routes>
      </div>
    </Router>
  );
};

export default App;
