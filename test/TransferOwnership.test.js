var VehicleOwnershipTransfer = artifacts.require("VehicleOwnershipTransfer");

//Use the contract to write all test
//variable: account => all accounts in blockchain
contract('VehicleOwnershipTransfer', (accounts) => {


    //Make sure contract is deployed and before we retrive the Transfer object for testing
    beforeEach(async () => {
        this.VehicleOwnershipTransfer = await VehicleOwnershipTransfer.deployed()
        const accounts = await web3.eth.getAccounts();
        from = accounts[0]; // Assuming the first account is the sender
        to = accounts[1]; // Assuming the second account is the recipient
    });
    //Testing the deployed vehicle contract
    it('deploys successfully', async () => {
        //Get the address which the vehicel object is stored
        const address = await this.VehicleOwnershipTransfer.address
        //Test for valid address
        isValidAddress(address)
    });
    it('test initiateTransfer', async () => {
        let transferContract;
        const from = accounts[0]; // Assuming the first account is the sender
        const to = accounts[1]; // Assuming the second account is the recipient
        const vehicleID = 123;
      
        return VehicleOwnershipTransfer.deployed().then((instance) => {
          transferContract = instance;
      
          return transferContract.initiateTransfer(to, vehicleID, { from: from });
        }).then(() => {
          return transferContract.transferCount();
        }).then((count) => {
          assert.equal(count, 1, "Transfer count should be incremented to 1");
          return transferContract.getTransferHistory(0);
        }).then((transfer) => {
          assert.equal(transfer.from, from, "Incorrect 'from' address");
          assert.equal(transfer.to, to, "Incorrect 'to' address");
          assert.equal(transfer.vehicleID, vehicleID, "Incorrect vehicle ID");
          assert.equal(transfer.confirmed, false, "Transfer should not be confirmed");
        });
      });
      

      it('test confirmTransfer', async () => {
        let transferContract;
        const from = accounts[0]; // Assuming the first account is the sender
        const to = accounts[1]; // Assuming the second account is the recipient
        const vehicleID = 123;
      
        return VehicleOwnershipTransfer.deployed().then((instance) => {
          transferContract = instance;
      
          return transferContract.transferCount();
        }).then((countValue) => {

          assert.equal(countValue, 1, "Transfer count should be 1 due to this being in same block as initiate transfer test case");
      
          return transferContract.initiateTransfer(to, vehicleID, { from: from });
        }).then(() => {
          return transferContract.transferCount();
        }).then((countValue) => {
          assert.equal(countValue, 2, "Transfer count should be incremented to 2 in this confirm test case beacuse we initiate transfer count second time");
      
          return transferContract.confirmTransfer({ from: to }); // Confirm the transfer from the recipient's account
        }).then(() => {
          return transferContract.getTransferHistory(1); // This is 1 becasue this is already the second intiation and confirmation
        }).then((transfer) => {
          assert.equal(transfer.confirmed, true, "Transfer should be confirmed");
        });
      });
      
      


})
//This function check if the address is valid
function isValidAddress(address) {
    assert.notEqual(address, 0x0)
    assert.notEqual(address, '')
    assert.notEqual(address, null)
    assert.notEqual(address, undefined)
}