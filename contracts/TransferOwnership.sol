// SPDX-License-Identifier:MIT

pragma solidity >=0.4.22 <0.9.0;

contract VehicleOwnershipTransfer {
    uint public transferCount = 0;
    struct Transfer {
        address from;
        address to;
        string vehicleID;
        bool confirmed;
    }

    mapping(uint => Transfer) public transfers;

    event InitiateTransfer(address indexed from, address indexed to, string indexed vehicleID);
    event ConfirmTransfer(address indexed from, address indexed to, string indexed vehicleID);

    function initiateTransfer(address _to, string memory _vehicleID) public {
        require(_to != address(0), "Invalid recipient address.");
        require(msg.sender != _to, "Cannot transfer to yourself.");

        uint transferId = transferCount;
        transfers[transferId] = Transfer(msg.sender, _to, _vehicleID, false);
        transferCount++;

        emit InitiateTransfer(msg.sender, _to, _vehicleID);
    }

    function confirmTransfer(address transferId) public {
        Transfer storage transfer = transfers[transferCount - 1];
        require(transfer.to == transferId, "Only the new owner can confirm the transfer.");
        require(!transfer.confirmed, "Transfer has already been confirmed.");

        // Perform additional checks or actions if needed before confirming the transfer

        transfer.confirmed = true;

        emit ConfirmTransfer(transfer.from, transfer.to, transfer.vehicleID);
    }

    function cancelTransfer(uint256 _transferId) external {
        require(_transferId < transferCount, "Transfer does not exist.");

        Transfer storage transfer = transfers[_transferId];
        require(!transfer.confirmed, "Cannot cancel a confirmed transfer.");

        // Remove the transfer from the mapping
        delete transfers[_transferId];

        // Decrement the transferCount
        transferCount--;
    }

    function getTransferHistory(uint _transferId) external view returns (address from, address to, string memory vehicleID, bool confirmed) {
        require(_transferId < transferCount, "Transfer does not exist.");

        Transfer storage transfer = transfers[_transferId];
        return (transfer.from, transfer.to, transfer.vehicleID, transfer.confirmed);
    }
}
